package biz.mket.uploader;

import java.io.File;
import java.util.ArrayList;
import java.util.Map.Entry;

import biz.mket.mkettools.R;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import biz.mket.http.MketAsyncHttp;
import biz.mket.http.MketHttp.UploadListener;
import biz.mket.http.MketHttpClientResponse;
import biz.mket.storage.MketExternalStorage;
import biz.mket.tools.MketTools;

public class MketUploaderService extends IntentService {
	public static final String EXTRA_IMAGE_FILE_PATHS = "image_file_paths";
	public static final String EXTRA_VIDEO_FILE_PATHS = "video_file_paths";
	public static final String EXTRA_POST_DATA_KEYS = "post_data_keys";
	public static final String EXTRA_POST_DATA_VALUES = "post_data_values";
	public static final String EXTRA_DATA_QUERY_KEYS = "query_keys";
	public static final String EXTRA_DATA_QUERY_VALUES = "query_values";
	public static final String EXTRA_NOTIFICATION = "notification";
	public static final String EXTRA_NOTIFICATION_TITLE = "notification_title";

	public static final int MODE_GET_FILE_FROM_CACHE = MketExternalStorage.MODE_EXTERNAL_CACHE;
	public static final int MODE_GET_FILE_FROM_STORAGE = MketExternalStorage.MODE_EXTERNAL_STORAGE;

	protected static final int _NOTIFICATION_ID = 1882;
	protected static final int _NOTIFICATION_DISPLAY_GAP = 25;

	protected NotificationManager _notificationManager;
	protected Notification _notification;
	protected String _notificationTitle;
	protected int _notificationGap = 0;

	public MketUploaderService() {
		super("MketUploaderService");
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		Uri uploadUri = workIntent.getData();
		Bundle extraData = workIntent.getExtras();

		ArrayList<String> dataKeys = extraData.getStringArrayList(EXTRA_POST_DATA_KEYS);
		ArrayList<String> dataValues = extraData.getStringArrayList(EXTRA_POST_DATA_VALUES);
		ArrayList<String> queryKeys = extraData.getStringArrayList(EXTRA_DATA_QUERY_KEYS);
		ArrayList<String> queryValues = extraData.getStringArrayList(EXTRA_DATA_QUERY_VALUES);
		ArrayList<String> imageFilePaths = extraData.getStringArrayList(EXTRA_IMAGE_FILE_PATHS);
		ArrayList<String> videoFilePaths = extraData.getStringArrayList(EXTRA_VIDEO_FILE_PATHS);

		boolean useNotification = extraData.getBoolean(EXTRA_NOTIFICATION, false);
		_notificationTitle = extraData.getString(EXTRA_NOTIFICATION_TITLE);
		if (_notificationTitle == null) {
			_notificationTitle = "업로드 중입니다";
		}

		Log.d("CM", "MketUploaderService uploadUri :" + String.valueOf(uploadUri));
		Log.d("CM", "MketUploaderService imageFilePaths :" + String.valueOf(imageFilePaths));
		Log.d("CM", "MketUploaderService videoFilePaths :" + String.valueOf(videoFilePaths));
		Log.d("CM", "MketUploaderService queryKeys :" + String.valueOf(queryKeys));
		Log.d("CM", "MketUploaderService queryValues :" + String.valueOf(queryValues));
		Log.d("CM", "MketUploaderService dataKeys :" + String.valueOf(dataKeys));
		Log.d("CM", "MketUploaderService dataValues :" + String.valueOf(dataValues));
		
		
		MketAsyncHttp http = new MketAsyncHttp(MketAsyncHttp.METHOD_POST);
		http.addHeader("X-CSRFToken", MketAsyncHttp.getCsrfToken());
		
		if (queryKeys != null && queryValues != null && queryKeys.size() == queryValues.size() && queryKeys.size() >= 1) {
			int length = queryKeys.size();
			for (int i = 0; i < length; i++) {
				String key = queryKeys.get(i);
				String value = queryValues.get(i);
				http.addQueryString(key, value);
			}
		}

		if (dataKeys != null && dataValues != null && dataKeys.size() == dataValues.size() && dataKeys.size() >= 1) {
			int length = dataKeys.size();
			for (int i = 0; i < length; i++) {
				String key = dataKeys.get(i);
				String value = dataValues.get(i);
				http.addMultipartValue(key, value);
				Log.d("CM", "MketUploaderService post data key:" + String.valueOf(key));
				Log.d("CM", "MketUploaderService post data value:" + String.valueOf(value));
			}
		}

		if (imageFilePaths != null) {
			int size = imageFilePaths.size();
			for (int i = 0; i < size; i++) {
				File file = new File(imageFilePaths.get(i));
				if (file.exists()) {
					http.addMultipartValue("image" + String.valueOf(i), file);
				}
			}
		}

		if (videoFilePaths != null) {
			int size = videoFilePaths.size();
			for (int i = 0; i < size; i++) {
				File file = new File(videoFilePaths.get(i));
				if (file.exists()) {
					http.addMultipartValue("video" + String.valueOf(i), file);
				}
			}
		}

		Log.d("CM", "MketUploaderService useNotification :" + String.valueOf(useNotification));
		Log.d("CM", "MketUploaderService uploadUri :" + uploadUri);
		Log.d("CM", "MketUploaderService uploadUri.toString() :" + uploadUri.toString());
		if (useNotification) {
			_notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			_buildNotification();
			http.listenMultipartUploadProgress(new UploadListener() {
				@Override
				public void onChange(int percent) {
					if ((percent / _NOTIFICATION_DISPLAY_GAP) >= _notificationGap) {
						_displayNotification(percent);
						_notificationGap += 1;
					}

					if (percent >= 100) {
						_displayNotification(percent);
						_cancelNotification();
					}
				}
			});
		}
		MketHttpClientResponse clientResponse = http.send(uploadUri.toString());
		http.clear();
		http = null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (_notificationManager != null) {
			_cancelNotification();
			_notificationManager = null;
		}
		_notification = null;
		_notificationTitle = null;
		_notificationGap = 0;

	}

	protected void _buildNotification() {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
		builder.setSmallIcon(R.drawable.mt_white_42);
		builder.setContentTitle(_notificationTitle);
		builder.setContentText("업로드중입니다");
		builder.setWhen(System.currentTimeMillis());
		builder.setProgress(100, 0, false);
		builder.setAutoCancel(true);
		_notification = builder.build();
		_notification.flags = _notification.flags | Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;

	}

	protected void _displayNotification(int percent) {
		_notification.contentView.setProgressBar(android.R.id.progress, 100, percent, false);
		_notificationManager.notify(_NOTIFICATION_ID, _notification);
	}

	protected void _cancelNotification() {
		if (_notificationManager != null) {
			_notificationManager.cancel(_NOTIFICATION_ID);
		}
	}

}
