package biz.mket.photo_dialog;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import biz.mket.image_cache.MketImageResizer;
import biz.mket.mkettools.R;
import biz.mket.storage.MketExternalFile;

public class PhotoDialog extends DialogFragment {

	public static PhotoDialog newInstance(Context context) {
		PhotoDialog instance = new PhotoDialog();
		instance._title = context.getString(R.string.mket_photodialog_title);

		return instance;
	}

	// Protected Static Constants
	protected static final int REQ_PICK_IMAGE = 13348;
	protected static final int REQ_TAKE_PICTURE = 13349;

	// Class Members
	public Context _context;
	public Fragment _fragment;
	public String _title;
	public String _imagePath;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		_context = activity;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String[] menu = new String[] { "앨범에서 선택하기", "사진 찍기" };

		AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
		b.setTitle(_title);
		b.setSingleChoiceItems(menu, 0, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == 0) {
					selectPhotoFromGallery();
				} else {
					takePicture();
				}

				dialog.dismiss();
			}
		});
		return b.create();
	}

	public String getImagePath() {
		return _imagePath;
	}

	public Bitmap getBitmap(int reqWidth, int reqHeight, boolean resize, boolean crop) {
		if (_imagePath != null) {

			// Decode bitmap with inSampleSize set
			Bitmap bitmap = MketImageResizer.decodeSampledBitmapFromFile(_imagePath, reqWidth, reqHeight, resize, crop);

			Log.d("CM", "PhotoDialog.getBitmap reqWidth" + reqWidth);
			Log.d("CM", "PhotoDialog.getBitmap reqHeight" + reqHeight);
			Log.d("CM", "PhotoDialog.getBitmap _imagePath" + _imagePath);
			Log.d("CM", "PhotoDialog.getBitmap bitmap" + bitmap);
			return bitmap;
		}
		return null;
	}

	public BitmapDrawable getBitmapDrawable(int reqWidth, int reqHeight, boolean resize, boolean crop) {
		Bitmap bitmap = getBitmap(reqWidth, reqHeight, resize, crop);
		if (bitmap != null) {
			return new BitmapDrawable(bitmap);
		}
		return null;
	}

	/**
	 * If you are calling this dialog in fragment, you might want to set the
	 * fragment.
	 * 
	 * @param fragment
	 */
	public void setFragment(Fragment fragment) {
		_fragment = fragment;
	}

	public void selectPhotoFromGallery() {
		Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		if (_fragment != null) {
			_fragment.startActivityForResult(intent, REQ_PICK_IMAGE);
		} else {
			getActivity().startActivityForResult(intent, REQ_PICK_IMAGE);
		}
	}

	public void takePicture() {
		File imageFile = MketExternalFile.createCachedFile(getActivity(), "mket_photodialog", "jpg");
		MketExternalFile.createParentDirectory(imageFile);
		_imagePath = imageFile.getAbsolutePath();

		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
		if (_fragment != null) {
			_fragment.startActivityForResult(takePictureIntent, REQ_TAKE_PICTURE);
		} else {
			getActivity().startActivityForResult(takePictureIntent, REQ_TAKE_PICTURE);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("CM", "PhotoDialog.onActivityResult requestCode: " + requestCode);
		Log.d("CM", "PhotoDialog.onActivityResult resultCode: " + resultCode);
		Log.d("CM", "PhotoDialog.onActivityResult data: " + data);

		if (requestCode == REQ_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
			Uri pictureUri = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Log.d("CM", "PhotoDialog.onActivityResult pictureUri:" + pictureUri);
			Log.d("CM", "PhotoDialog.onActivityResult filePathColumn:" + filePathColumn);

			Cursor cursor = _context.getContentResolver().query(pictureUri, filePathColumn, null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			// /storage/sdcard0/DCIM/Camera/1367664165879.jpg
			_imagePath = cursor.getString(columnIndex);
			Log.d("CM", "PhotoDialog.onActivityResult _imagePath:" + _imagePath);
			cursor.close();

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * 다이얼로그가 닫히면서 onDestory가 실행이 된다. 이때 여기서 instance members를 지우게 되면
	 * onActivityResult 에서 에러가 날수 있다. 대신에 destoryReally 메소드를 사용한다.
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();

	}

	public void destroyReally() {
		_context = null;
		_fragment = null;
		_title = null;
		_imagePath = null;
	}

}
