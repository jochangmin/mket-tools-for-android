package biz.mket.tools;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

public class MketMemoryUtils {
	public static View findRootView(Activity activity) {
		return activity.getWindow().getDecorView().findViewById(android.R.id.content);
	}

	public static void recursiveRecycle(View root, boolean recycleBitmap) {
		if (root == null)
			return;
		root.setBackgroundDrawable(null);
		if (root instanceof ViewGroup) {
			ViewGroup group = (ViewGroup) root;
			int count = group.getChildCount();
			for (int i = 0; i < count; i++) {
				recursiveRecycle(group.getChildAt(i), recycleBitmap);
			}

			if (!(root instanceof AdapterView)) {
				group.removeAllViews();
			}

		}

		if (root instanceof ImageView) {
			if (recycleBitmap) {
				MketMemoryUtils.recycleBitmap((ImageView) root);
			} else {
				root.setBackgroundDrawable(null);
			}
		}

		root = null;

		return;
	}

	public static void recycleBitmap(ImageView v) {
		if (v == null) {
			return;
		}
		Drawable drawable = v.getDrawable();
		if (drawable instanceof BitmapDrawable) {
			Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
			if (bitmap != null) {
				bitmap.recycle();
			}
		}
		if (drawable != null) {
			drawable.setCallback(null);
		}
		v.setBackgroundDrawable(null);
	}

}
