package biz.mket.tools;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import biz.mket.storage.MketExternalFile;

import android.content.Context;

public class MketFileTools {
	private static final String _TEMP_DIRECTORY_NAME = "mket_file_tools_temps";

	/**
	 * 
	 * @param context
	 * @param key
	 * @return a file path
	 */
	public static File createMketTempFile(Context context) {
		int count = 0;
		String filePath;
		File file;
		while (true) {
			String hashCode = MketExternalFile.hashKeyForDisk(String.valueOf(count));
			String timestamp = new SimpleDateFormat("yyyMMdd_HHmmss_SSS").format(new Date());
			filePath = MketExternalFile.join(_TEMP_DIRECTORY_NAME, hashCode + "_" + timestamp);
			file = new File(context.getExternalCacheDir(), filePath);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				break;
			}
			count += 1;
		}
		return file;
	}

	public static void removeMketTempFileDirectory(Context context) {
		File file = new File(context.getExternalCacheDir().getAbsolutePath());
		if (file.exists()) {
			file.delete();
		}
	}

	/**
	 * A hashing method that changes a string (like a URL) into a hash suitable for using as a disk filename.
	 */
	public static String createHashKey(String key) {
		String cacheKey;
		try {
			final MessageDigest mDigest = MessageDigest.getInstance("MD5");
			mDigest.update(key.getBytes());
			cacheKey = _bytesToHexString(mDigest.digest());
		} catch (NoSuchAlgorithmException e) {
			cacheKey = String.valueOf(key.hashCode());
		}
		return cacheKey;
	}

	private static String _bytesToHexString(byte[] bytes) {
		// http://stackoverflow.com/questions/332079
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(0xFF & bytes[i]);
			if (hex.length() == 1) {
				sb.append('0');
			}
			sb.append(hex);
		}
		return sb.toString();
	}

}
