package biz.mket.tools;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import biz.mket.http.MketAsyncHttp;
import biz.mket.http.MketHttpCallback;
import biz.mket.http.MketHttpClientResponse;
import biz.mket.mkettools.BuildConfig;

public class MketTools {
	public static final String EMAIL_REGEX = "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";

	public static String joinUrl(Context context, int rootUrlId, int relativeUrlId) {
		return MketTools.joinUrl(context.getString(rootUrlId), context.getString(relativeUrlId));
	}

	public static String joinUrl(String rootUrl, String relativeUrl) {
		URL url = null;
		String finalUrl = null;

		try {
			url = new URL(new URL(rootUrl), relativeUrl);
			finalUrl = url.toString();
			if (url.getQuery() != null) {
				finalUrl += "&" + url.getQuery();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}

		return finalUrl;
	}

//	public static void djangoDebugOnline(MketHttpClientResponse clientResponse){
//		djangoDebugOnline(clientResponse.content);
//	}
//	
//	public static void djangoDebugOnline(String title, MketHttpClientResponse clientResponse){
//		if(title != null && clientResponse != null){			
//			djangoDebugOnline(title + "\n"+ clientResponse.content);
//		}
//	}
//	
//	public static void djangoDebugOnline(String content) {
//		
//		if (BuildConfig.DEBUG) {
//			Log.d("CM", "djangoDebugOnline : " + String.valueOf(content.substring(0, content.length() < 100 ? content.length() : 100) + "...").replace("\n", ""));
//			MketAsyncHttp djangoHttp = new MketAsyncHttp(MketAsyncHttp.METHOD_POST);
//			djangoHttp.addNameValue("content", String.valueOf(content));
//			djangoHttp.listenPostExecute(new MketHttpCallback() {
//				
//				@Override
//				public void callback(MketHttpClientResponse clientResponse) {
//					Log.d("CM", "djangoDebugOnline callback: "+ clientResponse.code +" "+ clientResponse.content);
//					
//				}
//			});
//			djangoHttp.sendAsynchronously("http://ekrecipe.com/android/bugs/write/");
//		}
//	}
	

	public static boolean validateEmail(String email) {
		if (email.matches(EMAIL_REGEX)) {
			return true;
		}
		return false;
	}

	/**
	 * Get the size in bytes of a bitmap.
	 * 
	 * @param bitmap
	 * @return size in bytes
	 */
	@TargetApi(12)
	public static int getBitmapSize(Bitmap bitmap) {
		if (Utils.hasHoneycombMR1()) {
			return bitmap.getByteCount();
		}
		// Pre HC-MR1
		return bitmap.getRowBytes() * bitmap.getHeight();
	}

	/**
	 * Check if external storage is built-in or removable.
	 * 
	 * @return True if external storage is removable (like an SD card), false otherwise.
	 */
	@TargetApi(9)
	public static boolean isExternalStorageRemovable() {
		if (Utils.hasGingerbread()) {
			return Environment.isExternalStorageRemovable();
		}
		return true;
	}

	/**
	 * Get the external app cache directory.
	 * 
	 * @param context The context to use
	 * @return The external cache dir
	 */
	@TargetApi(8)
	public static File getExternalCacheDir(Context context) {
		if (Utils.hasFroyo()) {
			File externalCacheDir = context.getExternalCacheDir();
			if (externalCacheDir != null) {
				return externalCacheDir;
			}
		}
		// Before Froyo we need to construct the external cache dir ourselves
		final String cacheDir = "/Android/data/" + context.getPackageName() + "/cache/";
		File externalCacheDir = new File(Environment.getExternalStorageDirectory().getPath() + cacheDir);

		if (!externalCacheDir.exists()) {
			externalCacheDir.mkdirs();
		}
		return externalCacheDir;
	}

	/**
	 * Check how much usable space is available at a given path.
	 * 
	 * @param path The path to check
	 * @return The space available in bytes
	 */
	@TargetApi(9)
	public static long getUsableSpace(File path) {
		if (Utils.hasGingerbread()) {
			return path.getUsableSpace();
		}
		final StatFs stats = new StatFs(path.getPath());
		return (long) stats.getBlockSize() * (long) stats.getAvailableBlocks();
	}

	public static boolean isIntentAvailable(Context context, String action) {
		final PackageManager packageManager = context.getPackageManager();
		final Intent intent = new Intent(action);
		List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

	public static void hideKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	

}
