package biz.mket.tracker;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import android.R.integer;
import android.util.Log;
import biz.mket.datetime.TimeTools;
import biz.mket.http.MketAsyncHttp;
import biz.mket.http.MketHttpCallback;
import biz.mket.http.MketHttpClientResponse;
import biz.mket.tools.MketTools;

public class MketTracker {

	public static boolean _RUNNABLE = true;
	public static String _SERVER_URL = null;
	public static MketTracker _TRACKER_SINGLETON = null;
	public static String _SESSION_ID = null;
	public static Date _SESSION_DATE = null;
	public static String _USER_SERVER_ID = null;
	public static String _DEVELOPER = null;


	// Singleton Getter
	public static MketTracker getSingleton() {
		if (_TRACKER_SINGLETON != null) {
			return _TRACKER_SINGLETON;
		}

		MketTracker mketTracker = new MketTracker();
		return mketTracker;
	}

	public void startSession() {
		if (_RUNNABLE) {
			String md5 = _md5(String.valueOf((new Date()).getTime()));
			startSession(md5);
		}
	}

	public void startSession(String sessionId) {
		if (_RUNNABLE) {
			_SESSION_ID = sessionId;
			_SESSION_DATE = new Date();
		}
	}

	public void setRunnable(boolean runnable) {
		_RUNNABLE = runnable;
	}

	public void setUrl(String url) {
		if (_RUNNABLE) {
			_SERVER_URL = url;
		}
	}

	public void setUser(String serverId) {
		if (_RUNNABLE) {
			_USER_SERVER_ID = serverId;
		}
	}
	
	public void setDeveloper(Object developer){
		_DEVELOPER = String.valueOf(developer);
	}

	protected String _md5(String s) {
		if (_RUNNABLE) {
			try {
				// Create MD5 Hash
				MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
				digest.update(s.getBytes());
				byte messageDigest[] = digest.digest();

				// Create Hex String
				StringBuffer hexString = new StringBuffer();
				for (int i = 0; i < messageDigest.length; i++)
					hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
				return hexString.toString();

			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public void send(String name, MketHttpClientResponse client){
		if(client != null){
			send(name, client.content, client.code);			
		}
	}
	
	public void send(String name, int content){
		send(name, String.valueOf(content), 200);
	}
	
	public void send(String name, float content){
		send(name, String.valueOf(content), 200);
	}
	
	public void send(String name, boolean content){
		send(name, String.valueOf(content), 200);
	}
	
	public void send(String name, String content){
		send(name, String.valueOf(content), 200);
	}


	public void send(String name, String content, int code) {
		if(_SESSION_DATE ==  null){
			startSession();
		}
		
		if (_RUNNABLE) {
			int duration = (int) ((new Date()).getTime() - _SESSION_DATE.getTime());
			
			MketAsyncHttp http = new MketAsyncHttp(MketAsyncHttp.METHOD_POST);
			http.addCSRFToken();
			http.addNameValue("session", _SESSION_ID);
			//http.addNameValue("started", TimeTools.pythonTime(_SESSION_DATE));
			//http.addNameValue("registered", TimeTools.pythonTime());
			http.addNameValue("name", name);
			http.addNameValue("content", content);
			http.addNameValue("code", code);
			http.addNameValue("duration", duration);
			if(_USER_SERVER_ID != null){
				http.addNameValue("user", _USER_SERVER_ID);
			}
			if(_DEVELOPER != null){
				http.addNameValue("developer", _DEVELOPER);
			}
			http.listenDoInBackgroundListener(new MketHttpCallback() {
				
				@Override
				public void callback(MketHttpClientResponse clientResponse) {
					if(clientResponse != null){
						//Log.d("MketTracker", "MketTracker.send callback :"+ clientResponse.content);						
					}
				}
			});
			http.sendAsynchronously(_SERVER_URL);

		}
	}
}
;