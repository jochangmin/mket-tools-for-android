package biz.mket.http;

import android.os.AsyncTask;
import android.util.Log;

public class MketAsyncHttp extends MketHttp {

	private MketHttpCallback _doInBackgroundListener = null;
	private MketHttpCallback _postExcuteListener = null;

	public MketAsyncHttp() {
		super();
	}

	public MketAsyncHttp(int method) {
		super(method);
	}

	@Override
	public void clear() {
		super.clear();
		_postExcuteListener = null;
	}
	
	public void listenDoInBackgroundListener(MketHttpCallback listener){
		_doInBackgroundListener = listener;
	}

	public void listenPostExecute(MketHttpCallback listener) {
		_postExcuteListener = listener;
	}

	public void sendAsynchronously(String url) {
		Log.d("CM", "sendAsynchronously "+ url);
		_SenderAsyncTask task = new _SenderAsyncTask();
		task.execute(url);
	}

	protected class _SenderAsyncTask extends AsyncTask<String, Integer, MketHttpClientResponse> {

		@Override
		protected MketHttpClientResponse doInBackground(String... urls) {
			MketHttpClientResponse response;

			if (urls == null || urls.length <= 0) {
				return null;
			}

			String destinationUrl = urls[0];
			response = send(destinationUrl);
			
			if(	_doInBackgroundListener != null){
				_doInBackgroundListener.callback(response);
			}
			
			return response;
		}

		@Override
		protected void onPostExecute(MketHttpClientResponse clientResponse) {
			super.onPostExecute(clientResponse);
			if (_postExcuteListener != null) {
				_postExcuteListener.callback(clientResponse);
			}
		}

	}

}
