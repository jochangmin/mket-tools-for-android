package biz.mket.http;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import biz.mket.mkettools.BuildConfig;

public class MketHttp {
	public static interface UploadListener {
		public void onChange(int percent);
	}

	public static final int METHOD_GET = 0;
	public static final int METHOD_POST = 1;

	protected static final DefaultHttpClient _httpClient = new DefaultHttpClient();
	protected static final DefaultHttpClient _threadSafeHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(
			_httpClient.getParams(), _httpClient.getConnectionManager().getSchemeRegistry()), _httpClient.getParams());

	protected int _method = METHOD_GET;
	protected int _fileCount = 0;

	protected ArrayList<BasicHeader> _headers = new ArrayList<BasicHeader>();
	protected ArrayList<NameValuePair> _nameValuePairs = new ArrayList<NameValuePair>();
	protected MultipartEntity _multipartEntity;
	private Uri.Builder _stringQueryBuilder;

	protected UploadListener _uploaListener;

	public MketHttp() {
		_method = METHOD_GET;
	}

	public MketHttp(int method) {
		_method = method;
	}

	protected void _initMultipartEntity() {
		if (_multipartEntity == null) {

			_multipartEntity = new CountingMultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		}
	}

	protected void _initQueryStringEntity() {
		if (_stringQueryBuilder == null) {
			_stringQueryBuilder = new Uri.Builder();
		}
	}

	public boolean addCSRFToken() {
		String csrfToken = this.getCsrfToken();
		if (csrfToken != null && csrfToken.length() > 1) {
			this.addNameValue("X-CSRFToken", this.getCsrfToken());
			this.addHeader("X-CSRFToken", this.getCsrfToken());
			return true;
		}
		return false;
	}

	public boolean addHeader(String key, String value) {
		if (key == null || value == null) {
			return false;
		}
		_headers.add(new BasicHeader(key, value));
		return true;
	}

	public boolean addQueryString(String key, Object value) {
		if (key == null || value == null) {
			return false;
		}
		_initQueryStringEntity();

		_stringQueryBuilder.appendQueryParameter(key, String.valueOf(value));
		return true;
	}

	/**
	 * If you use Multipart form data, you are not allowed to use NamevaluePair.
	 * choose between Multipart and NamevaluePair
	 */
	public boolean addMultipartValue(String key, byte[] byteArray) {
		if (key == null || byteArray == null) {
			return false;
		}

		_initMultipartEntity();

		ByteArrayBody byteArrayBody = new ByteArrayBody(byteArray, "file" + String.valueOf(_fileCount));
		ByteArrayEntity byteArrayEntity = new ByteArrayEntity(byteArray);
		_multipartEntity.addPart(key, byteArrayBody);

		_fileCount += 1;
		return true;
	}

	public boolean addMultipartValue(String key, File file) {
		if (key == null || file == null) {
			return false;
		}

		_initMultipartEntity();

		FileBody fileBody = new FileBody(file);
		_multipartEntity.addPart(key, fileBody);
		return true;
	}

	/**
	 * If you use Multipart form data, you are not allowed to use NamevaluePair.
	 * choose between Multipart and NamevaluePair
	 */
	public boolean addMultipartValue(String key, Bitmap bitmap) {
		if (key == null || bitmap == null) {
			return false;
		}
		_initMultipartEntity();

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		return addMultipartValue(key, byteArray);
	}

	/**
	 * If you use Multipart form data, you are not allowed to use NamevaluePair.
	 * choose between Multipart and NamevaluePair
	 */
	public boolean addMultipartValue(String key, String data) {
		if (key == null || data == null) {
			return false;
		}

		_initMultipartEntity();

		try {
			_multipartEntity.addPart(key, new StringBody(data.toString(), Charset.forName("UTF-8")));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean addMultipartValue(String key, boolean data) {
		return addMultipartValue(key, String.valueOf(data));
	}
	
	public boolean addMultipartValue(String key, double data) {
		return addMultipartValue(key, String.valueOf(data));
	}

	public boolean addNameValue(String key, Object value) {
		if (_multipartEntity != null || value == null) {
			return false;
		}
		_nameValuePairs.add(new BasicNameValuePair(key, String.valueOf(value)));
		return true;
	}

	public void clear() {
		if (_headers != null) {
			_headers.clear();
			_headers = null;
		}

		if (_nameValuePairs != null) {
			_nameValuePairs.clear();
			_nameValuePairs = null;
		}
		if (_multipartEntity != null) {
			try {
				_multipartEntity.consumeContent();
			} catch (UnsupportedOperationException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			_multipartEntity = null;
		}

		if (_stringQueryBuilder != null) {
			// _stringQueryBuilder.clearQuery();
			_stringQueryBuilder = null;
		}
		_fileCount = 0;
	}

	public void listenMultipartUploadProgress(UploadListener listener) {
		_uploaListener = listener;
	}

	protected HttpGet _makeGETMethod(String url) {
		HttpGet get = new HttpGet(url);
		if (_stringQueryBuilder != null) {
			url = url + _stringQueryBuilder.toString();
		}
		try {
			get.setURI(new URI(url));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return get;
	}

	protected HttpPost _makePOSTMethod(String url) {
		HttpPost post = new HttpPost(url);
		if (_stringQueryBuilder != null) {
			url = url + _stringQueryBuilder.toString();
		}
		try {
			post.setURI(new URI(url));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return post;
	}

	protected HttpResponse _processHttpGet(String url) {
		HttpResponse response = null;
		HttpGet httpGet = _makeGETMethod(url);
		_addHeaders(httpGet);

		try {
			response = _threadSafeHttpClient.execute(httpGet);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	protected HttpResponse _processHttpPost(String url) {
		Log.d("CM", "MketHttp _processHttpPost start");
		HttpResponse response = null;
		HttpPost httpPost = _makePOSTMethod(url);
		_addHeaders(httpPost);
		Log.d("CM", "MketHttp _processHttpPost 02");
		try {
			if (_multipartEntity != null) {

				Log.d("CM", "MketHttp _processHttpPost multipart");
				httpPost.setEntity(_multipartEntity);
				Log.d("CM", "MketHttp _processHttpPost multipart set entity");
				response = _threadSafeHttpClient.execute(httpPost);
				Log.d("CM", "MketHttp _processHttpPost response:" + String.valueOf(response));
			} else if (_nameValuePairs != null) {
				httpPost.setEntity(new UrlEncodedFormEntity(_nameValuePairs, HTTP.UTF_8));
				response = _threadSafeHttpClient.execute(httpPost);
			}

		} catch (ClientProtocolException e) {
			if (BuildConfig.DEBUG) {
				Log.d("CM", "MketHttp _processHttpPost ClientProtocolException");
				e.printStackTrace();
			}
		} catch (IOException e) {
			if (BuildConfig.DEBUG) {
				Log.d("CM", "MketHttp _processHttpPost IOException");
				e.printStackTrace();
			}
		}

		Log.d("CM", "MketHttp _processHttpPost response:" + String.valueOf(response));

		return response;
	}

	protected void _addHeaders(HttpRequestBase httpRequest) {
		if (_headers != null) {
			for (BasicHeader header : _headers) {
				httpRequest.addHeader(header);
			}
		}
	}

	/**
	 * it actually sends a HTTP request to Server
	 */
	public MketHttpClientResponse send(String url) {
		Log.d("CM", "send 01");
		if (url == null) {
			return null;
		}

		HttpResponse response = null;
		MketHttpClientResponse clientResponse = new MketHttpClientResponse();

		// make HTTP Method with Query Strings.
		if (_method == METHOD_POST) {
			response = _processHttpPost(url);
		} else {
			response = _processHttpGet(url);
		}
		if (response == null) {
			return null;
		}

		clientResponse.code = response.getStatusLine().getStatusCode();
		try {
			clientResponse.content = _inputStreamToString(response.getEntity().getContent()).trim();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (response != null && response.getEntity() != null) {
				try {
					response.getEntity().consumeContent();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return clientResponse;
	}

	protected String _inputStreamToString(InputStream is) {
		Log.d("CM", "_inputStreamToString 01 is" + String.valueOf(is));
		StringBuilder total = new StringBuilder();
		// String NL = System.getProperty("line.separator");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

		try {
			// Read response until the end
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				total.append(line);
			}

		} catch (Exception e) {
			Log.d("CM", "_inputStreamToString Exception 01");
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		if (is != null) {
			try {
				is.close();
			} catch (IOException e) {
				Log.d("CM", "_inputStreamToString Exception 02");
				e.printStackTrace();
			}
		}

		if (bufferedReader != null) {
			try {
				bufferedReader.close();
			} catch (IOException e) {
				Log.d("CM", "_inputStreamToString Exception 03");
				e.printStackTrace();
			}

		}

		// Return full string
		return total.toString();
	}

	/** Helper Static Methods */
	public static void setCookie(String key, String value) {
		// CookieStore cookieStore = MketAsyncHttp.getCookieStore();
		// cookieStore.addCookie(new BasicClientCookie(key, value));
		_threadSafeHttpClient.getCookieStore().addCookie(new BasicClientCookie(key, value));
		_threadSafeHttpClient.setCookieStore(_threadSafeHttpClient.getCookieStore());
	}

	public static CookieStore getCookieStore() {
		return MketHttp._threadSafeHttpClient.getCookieStore();
	}

	public static HashMap<String, String> getCookies() {
		HashMap<String, String> cookieHashMap = new HashMap<String, String>();
		List<Cookie> cookies = MketHttp.getCookieStore().getCookies();
		for (int i = 0; i < cookies.size(); i++) {
			Cookie cookie = cookies.get(i);
			cookieHashMap.put(cookie.getName(), cookie.getValue());
		}
		return cookieHashMap;
	}

	public static String getSessionId() {
		return MketHttp.getCookies().get("sessionid");
	}

	public static String getCsrfToken() {
		return MketHttp.getCookies().get("csrftoken");
	}

	/**
	 * Simple network connection check.
	 */
	public static boolean checkConnection(Context context) {
		final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
			return false;
		}
		return true;
	}

	public class CountingMultipartEntity extends MultipartEntity {

		public CountingMultipartEntity(HttpMultipartMode mode) {
			super(mode);
		}

		public CountingMultipartEntity(HttpMultipartMode mode, String boundary, Charset charset) {
			super(mode, boundary, charset);
		}

		@Override
		public void writeTo(OutputStream outstream) throws IOException {
			super.writeTo(new CountingBufferedOutputStream(outstream, getContentLength()));
		}

	}

	protected class CountingBufferedOutputStream extends BufferedOutputStream {
		protected OutputStream outputStream;
		protected long sentLength = 0;
		protected long contentLength;

		public CountingBufferedOutputStream(OutputStream outputStream, long contentLength) {
			super(outputStream);
			this.outputStream = outputStream;
			this.contentLength = contentLength;
		}

		@Override
		public synchronized void write(byte[] buffer, int offset, int length) throws IOException {
			super.write(buffer, offset, length);
			sentLength += length;

			// Log.d("CM", "CountingBufferedOutputStream offset:" +
			// String.valueOf(offset) + " length:" +
			// String.valueOf(length)
			// + " getContentLength:" + String.valueOf(contentLength) +
			// " counter:" + String.valueOf(counter));

			if (contentLength <= sentLength) {
				// Log.d("CM", "CountingBufferedOutputStream FLUSH");
				flush();
			}

			if (_uploaListener != null) {
				int percent = (int) ((sentLength * 100) / contentLength);
				_uploaListener.onChange(percent);
			}

		}

	}

}
