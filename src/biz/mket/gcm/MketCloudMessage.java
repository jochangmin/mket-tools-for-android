package biz.mket.gcm;

import java.io.IOException;

import org.apache.http.HttpStatus;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;
import biz.mket.http.MketAsyncHttp;
import biz.mket.http.MketHttp;
import biz.mket.http.MketHttpCallback;
import biz.mket.http.MketHttpClientResponse;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class MketCloudMessage {
	public static interface GCMRegisterCallback {
		public void gcmRegisterCallback(String registrationId);
	}

	protected static MketCloudMessage _SINGLETON;
	protected static final String SHARED_PREF_FILE_NAME = "mket_gcm_pref";
	protected static final String PREF_REGISTRATION_ID = "registration_id";
	protected static final String PREF_APP_VERSION = "app_version";

	protected static final int PLAY_SERVICES_RESOLUTION_REQUEST = 368133;

	protected Context _context;
	protected GoogleCloudMessaging _gcm;
	protected String _gcm_server_id;
	protected String _registration_id;
	protected String _server_url;

	public static MketCloudMessage getSingleton(Context context) {
		if (_SINGLETON == null) {
			_SINGLETON = new MketCloudMessage(context);
		}
		return _SINGLETON;
	}

	public MketCloudMessage(Context context) {
		_context = context;
		_gcm = GoogleCloudMessaging.getInstance(_context);
	}

	/**
	 * Retrieve the registration ID from the preference file. if not found,
	 * register the server API KEY to Google and then get the registration ID
	 * from the Google.
	 */
	public String getRegistrationId() {
		if (_registration_id != null) {
			return _registration_id;
		}

		final SharedPreferences prefs = _getGcmPreferences(_context);
		_registration_id = prefs.getString(PREF_REGISTRATION_ID, "");
		if (_registration_id.isEmpty()) {
			Log.i("CM", "MketGCM.getRegisterId Registration not found.");
			return null;
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PREF_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = _getAppVersion(_context);
		if (registeredVersion != currentVersion) {
			Log.i("CM", "MketGCM.getRegisterId  App version changed.");
			return null;
		}
		return _registration_id;
	}

	/**
	 * Registers the application with GCM servers asynchronously. Stores the
	 * registration ID and the app versionCode in the application's shared
	 * preferences. After getting registration ID the method will send the
	 * registration ID to Backend Server.
	 * 
	 * GCM 서버에 Google API Server Key 를 등록시킨다(Async). 이후 registration ID와 app
	 * Version을 preference file에 등록을 시킨다. 이후 registration ID를 Backend Server
	 * (Pastel, Level9, etc..) 등록시키기 위해서 Http로 보낸다.
	 */
	public void registerInBackground(String apiServerId, String serverUrl, GCMRegisterCallback callback) {
		_gcm_server_id = apiServerId;
		_server_url = serverUrl;

		_MketGCMRegisterAsync task = new _MketGCMRegisterAsync(callback);
		task.execute();
	}

	/**
	 * 
	 * @param registrationId
	 */
	protected void _sendRegistrationIdToBackend(String registrationId) {

		MketHttp http = new MketHttp(MketAsyncHttp.METHOD_POST);
		http.addNameValue("registration_id", registrationId);
		MketHttpClientResponse response = http.send(_server_url);
		if (response.code == HttpStatus.SC_OK) {
			Log.d("CM", "MketCloudMessage GCM Registration ID has successfully been sent.");
		} else {
			Log.d("CM", "MketCloudMessage Registering GCM Registration ID to Backend Server has failed");
		}

	}

	/**
	 * Stores the registration ID and the app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	protected void _storeRegistrationId() {
		final SharedPreferences prefs = _getGcmPreferences(_context);
		int appVersion = _getAppVersion(_context);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PREF_REGISTRATION_ID, _registration_id);
		editor.putInt(PREF_APP_VERSION, appVersion);
		editor.commit();
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	protected SharedPreferences _getGcmPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but how you store the regID in your app is up to you.

		return _context.getSharedPreferences(SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
	}

	public void destroy() {
		_SINGLETON = null;
		_context = null;
		_gcm = null;
		_gcm_server_id = null;
		_registration_id = null;
		_server_url = null;
	}

	protected class _MketGCMRegisterAsync extends AsyncTask<Void, Void, String> {

		protected GCMRegisterCallback _registerCallback;

		public _MketGCMRegisterAsync(GCMRegisterCallback callback) {
			_registerCallback = callback;
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				if (_gcm == null) {
					_gcm = GoogleCloudMessaging.getInstance(_context);
				}
				_registration_id = _gcm.register(_gcm_server_id);
				Log.d("CM", "_MketGCMRegisterAsync Device registered, registration ID=" + _registration_id);

				// You should send the registration ID to your server over
				// HTTP, so it
				// can use GCM/HTTP or CCS to send messages to your app.
				_sendRegistrationIdToBackend(_registration_id);

				// For this demo: we don't need to send it because the
				// device will send
				// upstream messages to a server that echo back the message
				// using the
				// 'from' address in the message.

				// Persist the regID - no need to register again.
				_storeRegistrationId();
			} catch (IOException ex) {
				Log.d("CM", "_MketGCMRegisterAsync Error :" + ex.getMessage());

				// If there is an error, don't just keep trying to register.
				// Require the user to click a button again, or perform
				// exponential back-off.
			}
			return _registration_id;
		}

		@Override
		protected void onPostExecute(String registrationId) {
			super.onPostExecute(registrationId);

			if (_registerCallback != null) {
				_registerCallback.gcmRegisterCallback(registrationId);
			}

			_registerCallback = null;
		}
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	protected static int _getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

}
