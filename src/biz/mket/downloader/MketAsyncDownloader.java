package biz.mket.downloader;

import java.io.FileOutputStream;
import java.io.IOException;


import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

public class MketAsyncDownloader extends MketDownloader {
	

	public static abstract class DownloadBitmapListener {
		public abstract void run(Bitmap bitmap);
	}
	
	public static abstract class DownloadBitmapToFileListener {
		public abstract void run(FileOutputStream outputStream);
	}
	
	private DownloadBitmapListener _downloadBitmapListener;
	private DownloadBitmapToFileListener _downloadBitmapToFileListener;

	public void downloadBitmapAsynchronously(String url) {
		_DownloadBitmapTask task = new _DownloadBitmapTask();
		task.execute(url);
	}
	
	public void downloadBitmapAsynchronously(String url, FileOutputStream outputStream){
		_DownloadBitmapToFileTask task = new _DownloadBitmapToFileTask(outputStream);
		task.execute(url);
	}

	public void listenDownloadBitmapAsync(DownloadBitmapListener listener) {
		_downloadBitmapListener = listener;
	}
	
	public void listenDownloadBitmapToFileAsync(DownloadBitmapToFileListener listener){
		_downloadBitmapToFileListener = listener;
	}
	
	
	public void removeAllListeners(){
		_downloadBitmapListener = null;
		_downloadBitmapToFileListener = null;
	}

	private class _DownloadBitmapTask extends AsyncTask<String, Integer, Bitmap> {
		@Override
		protected Bitmap doInBackground(String... params) {
			String url = params[0];
			Bitmap bitmap = downloadBitmap(url);
			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			super.onPostExecute(bitmap);

			if (_downloadBitmapListener != null) {
				_downloadBitmapListener.run(bitmap);
			}
		}
	}
	
	private class _DownloadBitmapToFileTask extends AsyncTask<String, Integer, FileOutputStream> {
		protected FileOutputStream _outputStream;
		
		public _DownloadBitmapToFileTask(FileOutputStream outputStream) {
			_outputStream = outputStream;
		}
		
		@Override
		protected FileOutputStream doInBackground(String... params) {
			String url = params[0];
			download(url, _outputStream);
			return _outputStream;
		}

		@Override
		protected void onPostExecute(FileOutputStream fileOutputStream) {
			super.onPostExecute(fileOutputStream);
			try {
				_outputStream.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(_downloadBitmapToFileListener != null){
				_downloadBitmapToFileListener.run(fileOutputStream);
			}
			
			_outputStream = null;
		}
	}
}