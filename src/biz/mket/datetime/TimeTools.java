package biz.mket.datetime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import android.util.Log;

public class TimeTools {

	public static Date pythonUtc(String timestamp) {
		// "2014-05-23 10:45:46+00:00";
		Log.d("CM", "TimeTools.pythonUtc: "+ timestamp);
		// Get Current TimeZone
		
		Calendar cal = Calendar.getInstance();
		TimeZone currentTimezone = cal.getTimeZone();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
		dateFormat.setTimeZone(currentTimezone);
		dateFormat.setLenient(true);
		try {
			return dateFormat.parse(timestamp);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static String pythonTime(Date date){
		Calendar cal = Calendar.getInstance();
		//TimeZone currentTimezone = cal.getTimeZone();
		TimeZone currentTimezone = TimeZone.getTimeZone("UTC");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(currentTimezone);
		dateFormat.setLenient(true);
		
		return dateFormat.format(date);
	}
	
	public static String pythonTime(){
		return pythonTime(new Date());
	}
	
	public static String ago(Date date) {
		long seconds = ((new Date()).getTime() - date.getTime()) / 1000l;
		String response = "";
		if (seconds < 60) {
			response = seconds + "초전";
		} else if (seconds < 3600) {
			response = seconds / 60 + "분전";
		} else if (seconds < 86400) {
			response = seconds / 3600 + "시간전";
		} else if (seconds < 2678400000l) {
			// 86400000 is a day in miliseconds
			response = seconds / 86400000 + "일전";
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("yy년 MM월 dd일 HH시 mm분");
			response = sdf.format(date);
		}
		return response;
	}

}
