package biz.mket.map;

import android.location.Address;

public class MketMapTools {
	public static String getFullAddress(Address address) {
		if (address == null) {
			return null;
		}

		StringBuilder builder = new StringBuilder();
		String adminArea = address.getAdminArea();
		String locality = address.getLocality();
		String subLocality = address.getSubLocality();
		String thoroughfare = address.getThoroughfare();
		String subThoroughfare = address.getSubThoroughfare();

		builder.append(adminArea); // 경기도
		if (locality != null) {
			builder.append(" " + locality); // 고양시
		}
		if (subLocality != null) {
			builder.append(" " + subLocality); // 일산구
		}
		if (thoroughfare != null) {
			builder.append(" " + thoroughfare); // 풍동
		}
		if (subThoroughfare != null) {
			builder.append(" " + subThoroughfare); // 450-7
		}

		// builder.append(address.getFeatureName()); // 450-7

		return builder.toString();
	}

}
