package biz.mket.map;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.google.android.gms.drive.internal.s;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.R.integer;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;
import biz.mket.mkettools.R;

public class MketMapActivity extends FragmentActivity {
	public static final String RETURN_FULL_ADDRESS = MketMapFragment.RETURN_FULL_ADDRESS;
	public static final String RETURN_POSTCODE = MketMapFragment.RETURN_POSTCODE;
	public static final String RETURN_LATITUDE = MketMapFragment.RETURN_LATITUDE;
	public static final String RETURN_LONGITUDE = MketMapFragment.RETURN_LONGITUDE;

	protected static String MAP_FRAGMENT_TAG = "mket_map_fragment";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mket_map_activity);

		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();

		MketMapFragment fragment = new MketMapFragment();
		ft.replace(R.id.mket_map_root_framelayout, fragment, MAP_FRAGMENT_TAG);
		ft.disallowAddToBackStack();
		ft.commit();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mket_map_menu, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.mket_map_menu_complete) {
			completeMap();
		}

		return super.onOptionsItemSelected(item);
	}

	public void completeMap() {
		FragmentManager fm = getSupportFragmentManager();
		MketMapFragment fragment = (MketMapFragment) fm.findFragmentByTag(MAP_FRAGMENT_TAG);

		Bundle bundle = fragment.getResult();

		if (bundle != null) {
			String fullAddress = bundle.getString(fragment.RETURN_FULL_ADDRESS);
			String postcode = bundle.getString(fragment.RETURN_POSTCODE);
			double latitude = bundle.getDouble(fragment.RETURN_LATITUDE);
			double longitude = bundle.getDouble(fragment.RETURN_LONGITUDE);

			Intent returnIntent = new Intent();
			returnIntent.putExtra(RETURN_FULL_ADDRESS, fullAddress);
			returnIntent.putExtra(RETURN_POSTCODE, postcode);
			returnIntent.putExtra(RETURN_LATITUDE, latitude);
			returnIntent.putExtra(RETURN_LONGITUDE, longitude);
			setResult(RESULT_OK, returnIntent);
			finish();
			return;
		}
		Toast.makeText(this, "주소를 넣어주세요", Toast.LENGTH_SHORT).show();
	}
}
