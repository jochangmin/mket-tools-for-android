package biz.mket.map;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import biz.mket.mkettools.R;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MketMapFragment extends Fragment implements LocationListener {
	public static final String RETURN_FULL_ADDRESS = "full_address";
	public static final String RETURN_POSTCODE = "post_code";
	public static final String RETURN_LATITUDE = "latitude";
	public static final String RETURN_LONGITUDE = "longitude";

	public EditText _addressView;
	public Button _searchButton;
	public GoogleMap _map;
	public Geocoder _geo;
	public boolean _hasMoved = false;

	public Address _address;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		container = (LinearLayout) inflater.inflate(R.layout.mket_map_fragment, null);
		_addressView = (EditText) container.findViewById(R.id.mket_map_address);
		_searchButton = (Button) container.findViewById(R.id.mket_map_search_button);
		ImageView deleteAllView = (ImageView) container.findViewById(R.id.mket_map_delete_all_text_button);

		// Set Search Listener
		_searchButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				hideKeyboard();
				String locationName = _addressView.getText().toString();
				Address address = searchAddress(locationName);
				if (address == null) {
					return;
				}
				String fullAddressName = MketMapTools.getFullAddress(address);
				_addressView.setText(fullAddressName);

				LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
				move(latLng);
				drawMarker(latLng);
				saveAddress(address);

			}
		});

		// Set Delete All Text Listener
		deleteAllView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				hideKeyboard();
				_addressView.setText(null);
			}
		});

		initLocation();

		return container;
	}

	public void initLocation() {
		if (_map == null) {
			SupportMapFragment mapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.mket_map_fragment);
			_map = mapFragment.getMap();
		}

		_map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
			@Override
			public void onMapLoaded() {
				_initMap();
			}
		});
	}

	public void _initMap() {
		_map.getUiSettings().setMyLocationButtonEnabled(true);

		_geo = new Geocoder(getActivity().getApplicationContext(), Locale.KOREA);

		Context context = getActivity();
		LocationManager locationManager = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

		// Listeners

		_map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

			@Override
			public void onMapClick(LatLng latLng) {
				move(latLng);
				drawMarker(latLng);
				Address address = getAddress(latLng);
				if (address != null) {
					String fullAddressName = MketMapTools.getFullAddress(address);
					_addressView.setText(fullAddressName);
				}

			}
		});

	}

	@Override
	public void onLocationChanged(Location location) {
		if (!_hasMoved && _map != null && _geo != null) {
			LatLng position = new LatLng(location.getLatitude(), location.getLongitude());

			Address address = getAddress(position);
			if (address != null) {
				String fullAddressName = MketMapTools.getFullAddress(address);
				_addressView.setText(fullAddressName);
				saveAddress(address);
			}

			drawMarker(position);
			_hasMoved = true;

		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (_map != null) {
			_map.clear();
			_map = null;
		}

		_addressView = null;
		_searchButton = null;

		_geo = null;
		_address = null;

		System.gc();
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void drawMarker(LatLng latLng) {
		Context context = getActivity();

		_map.clear();
		_map.addMarker(new MarkerOptions().position(latLng).snippet("Lat:" + latLng.latitude + "Lng:" + latLng.longitude));

		move(latLng);
	}

	public void move(LatLng position) {
		CameraPosition cameraPosition = _map.getCameraPosition();
		float zoom = cameraPosition.zoom;
		if (zoom < 15) {
			zoom = 15;
		}

		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(position, zoom);
		_map.animateCamera(cameraUpdate);
	}

	public Address getAddress(LatLng latLng) {
		List<Address> addresses;
		try {
			addresses = _geo.getFromLocation(latLng.latitude, latLng.longitude, 1);
			if (addresses.size() > 0) {
				int addressBestScore = 0;
				Address address = null;

				for (Address a : addresses) {
					int addressScore = 0;
					if (a.getPostalCode() != null) {
						addressScore += 3;
					}

					if (a.getAdminArea() != null) {
						addressScore += 1;
					}

					if (a.getLocality() != null) {
						addressScore += 1;
					}

					if (a.getSubLocality() != null) {
						addressScore += 1;
					}

					if (a.getThoroughfare() != null) {
						addressScore += 1;
					}

					if (a.getSubThoroughfare() != null) {
						addressScore += 1;
					}

					if (addressScore > addressBestScore) {
						address = a;
						addressBestScore = addressScore;
					}
				}

				return address;

				// Toast.makeText(getApplicationContext(), "Address:- " +
				// addresses.get(0).getFeatureName() +
				// addresses.get(0).getAdminArea() +
				// addresses.get(0).getLocality(), Toast.LENGTH_LONG).show();
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	public Address searchAddress(String locationName) {
		List<Address> addresses;
		try {
			addresses = _geo.getFromLocationName(locationName, 1);
			if (addresses.size() >= 1) {
				Address address = addresses.get(0);
				return address;

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public void hideKeyboard() {

		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(_addressView.getWindowToken(), 0);
	}

	public void saveAddress(Address address) {
		if (address != null) {
			_address = address;
		}

	}

	public Bundle getResult() {
		Bundle bundle = null;
		String fullAddress = MketMapTools.getFullAddress(_address);

		if (_address != null && fullAddress != null) {
			bundle = new Bundle();
			bundle.putString(RETURN_FULL_ADDRESS, fullAddress);
			bundle.putString(RETURN_POSTCODE, _address.getPostalCode());
			bundle.putDouble(RETURN_LATITUDE, _address.getLatitude());
			bundle.putDouble(RETURN_LONGITUDE, _address.getLongitude());

		}

		return bundle;
	}

}
