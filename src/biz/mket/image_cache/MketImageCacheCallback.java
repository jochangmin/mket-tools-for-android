package biz.mket.image_cache;

import android.graphics.Bitmap;
import android.widget.ImageView;

public interface MketImageCacheCallback {
	
	public void callback(Bitmap bitmap, String imageKey, int recommendedHeight);

}
