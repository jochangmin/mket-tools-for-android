/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package biz.mket.image_cache;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.util.Patterns;
import android.widget.Toast;
import biz.mket.downloader.MketDownloader;
import biz.mket.http.MketAsyncHttp;
import biz.mket.mkettools.R;
import biz.mket.storage.MketExternalFile;
import biz.mket.storage.MketExternalStorage;

/**
 * A simple subclass of {@link ImageResizer} that fetches and resizes images
 * fetched from a URL.
 */
public class MketImageFetcher extends MketImageResizer {
	private static final int HTTP_CACHE_SIZE = 16 * 1024 * 1024; // 16MB
	private static final String HTTP_CACHE_DIR = "http";
	private static final int IO_BUFFER_SIZE = 8 * 1024;
	private static final long LEAST_USABLE_SPACE = 128 * 1024 * 1024;
	private static final long LEAST_FILE_SIZE = 3 * 1024; // a file size which
															// is less than 3kb
															// will be
															// re-downloaded as
															// it does not have
															// proper image size

	private static enum _TYPES {
		URL, FILE, RESOURCE
	}

	private static Object _httpDiskCacheLock = new Object();
	private static boolean isHttpInProgress = false;

	private MketExternalStorage _httpStorage;
	private MketExternalFile _httpDir;

	/**
	 * Initialize providing a target image width and height for the processing
	 * images.
	 * 
	 * @param context
	 * @param imageWidth
	 * @param imageHeight
	 */
	public MketImageFetcher(Context context) {
		super(context);

	}

	private void init(Context context) {
		checkConnection(context);
	}

	@Override
	protected void _initDiskCacheInternal() {
		super._initDiskCacheInternal();
		_initHttpDiskCache();
	}

	protected void _initHttpDiskCache() {
		if (_httpStorage == null) {
			_httpStorage = new MketExternalStorage(_context, MketExternalStorage.MODE_EXTERNAL_CACHE);
			_httpDir = new MketExternalFile(_context, HTTP_CACHE_DIR, MketExternalFile.MODE_EXTERNAL_CACHE_FILE);

			if (!_httpDir.exists()) {
				_httpDir.getFile().mkdirs();
			}
		}
	}

	@Override
	protected void _clearCacheInternal() {
		super._clearCacheInternal();
		synchronized (_httpDiskCacheLock) {

			if (_httpDir != null) {
				_httpDir.delete();
				_httpDir.close();
			}
			_httpDir = null;

			_httpStorage = null;
			_httpDir = null;

			_initHttpDiskCache();
			_httpDiskCacheLock.notifyAll();
		}
	}

	@Override
	protected void _flushCacheInternal() {
		super._flushCacheInternal();

	}

	@Override
	protected void _closeCacheInternal() {
		super._closeCacheInternal();
		synchronized (_httpDiskCacheLock) {

			if (_httpDir != null) {
				_httpDir.close();
				_httpDir = null;
			}
			_httpStorage = null;
			_httpDiskCacheLock.notifyAll();
		}
	}

	/**
	 * Simple network connection check.
	 * 
	 * @param context
	 */
	private void checkConnection(Context context) {
		if (!MketAsyncHttp.checkConnection(context)) {
			Toast.makeText(context, R.string.mket_no_network_connection, Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * The main process method, which will be called by the ImageWorker in the
	 * AsyncTask background thread.
	 * 
	 * @param key
	 *            The string url to download image file on the web
	 * @return The downloaded and resized bitmap
	 */
	@Override
	protected Bitmap processBitmap(String url, int width, int height, int round, boolean resize, boolean crop) {
		_TYPES type = _detectImageType(url);
		Bitmap bitmap = null;
		if (type == _TYPES.URL) {
			bitmap = _processURLBitmap(url, width, height, round, resize, crop);
		} else if (type == _TYPES.FILE) {
			bitmap = _processFileBitmap(url, width, height, round, resize, crop);
		} else if (type == _TYPES.RESOURCE) {
			bitmap = _processResourceBitmap(Integer.valueOf(url), width, height, round, resize, crop);
		}

		// Set Rounded Border
		if (bitmap != null && round > 0) {
			bitmap = MketImageResizer.roundBitmapCorner(bitmap, round);
		}

		return bitmap;
	}

	protected Bitmap _processURLBitmap(String url, int width, int height, int round, boolean resize, boolean crop) {

		String hashKey = MketImageCache.hashKeyForDisk(url);
		FileInputStream fileInputStream = null;
		FileDescriptor fileDescriptor = null;

		synchronized (_httpDiskCacheLock) {
			String imageFilePath = MketExternalFile.join(HTTP_CACHE_DIR, hashKey);
			File imageFile = new File(_context.getExternalCacheDir(), imageFilePath);

			try {
				long fileSizeByte = imageFile.length();
				if (!imageFile.exists() && fileSizeByte < LEAST_FILE_SIZE) {

					if (MketDownloader.download(url, new FileOutputStream(imageFile))) {
						fileInputStream = new FileInputStream(imageFile);
						fileDescriptor = fileInputStream.getFD();
					} else {
						imageFile.delete();
						fileInputStream = null;
						fileDescriptor = null;
					}
				} else {
					fileInputStream = new FileInputStream(imageFile);
					fileDescriptor = fileInputStream.getFD();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fileDescriptor == null && fileInputStream != null) {
					try {
						fileInputStream.close();
					} catch (IOException e) {
					}
				}
			}
		}

		Bitmap bitmap = null;
		if (fileDescriptor != null) {
			bitmap = decodeSampledBitmapFromDescriptor(fileDescriptor, width, height, resize, crop);
		}
		if (fileInputStream != null) {
			try {
				fileInputStream.close();
				fileInputStream = null;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// Set Rounded Border
		if (bitmap != null && round > 0) {
			bitmap = MketImageResizer.roundBitmapCorner(bitmap, round);
		}

		fileDescriptor = null;
		fileInputStream = null;
		hashKey = null;
		url = null;
		return bitmap;
	}

	protected Bitmap _processFileBitmap(String filename, int width, int height, int round, boolean resize, boolean crop) {
		Bitmap bitmap = MketImageResizer.decodeSampledBitmapFromFile(filename, width, height, resize, crop);

		return bitmap;
	}

	protected Bitmap _processResourceBitmap(int resId, int width, int height, int round, boolean resize, boolean crop) {
		Bitmap bitmap = MketImageResizer.decodeSampledBitmapFromResource(_context.getResources(), resId, width, height, 0, resize, crop);
		return bitmap;
	}

	/**
	 * Workaround for bug pre-Froyo, see here for more info:
	 * http://android-developers.blogspot.com/2011/09/androids-http-clients.html
	 */
	public static void disableConnectionReuseIfNecessary() {
		// HTTP connection reuse which was buggy pre-froyo
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
			System.setProperty("http.keepAlive", "false");
		}
	}

	protected _TYPES _detectImageType(String image) {
		if (Patterns.WEB_URL.matcher(image).matches()) {
			return _TYPES.URL;
		} else if (image != null && (new File(image)).exists()) {
			return _TYPES.FILE;
		}

		else if (image != null && image.matches("^\\d+$")) {
			return _TYPES.RESOURCE;
		}
		
		Uri uri = Uri.parse(image);
		
		return null;
	}

}
