/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package biz.mket.image_cache;

import java.io.FileDescriptor;
import java.lang.ref.WeakReference;
import java.util.HashMap;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

import android.R.integer;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.util.DisplayMetrics;
import android.util.Log;

/**
 * A simple subclass of {@link ImageWorker} that resizes images from resources
 * given a target width and height. Useful for when the input images might be
 * too large to simply load directly into memory.
 */
public class MketImageResizer extends MketImageCacheAsync {
	public static int MAXIMUM_TEXTURE_SIZE = 0; // Maximum size of images

	// protected int _requestedWidth;
	// protected int _requestedHeight;
	// protected boolean _resizeAccurately = false;
	// protected boolean _cropAccurately = false;
	// protected OnResizeProcess _onResizeProcess;

	/**
	 * Initialize providing a single target image size (used for both width and
	 * height);
	 * 
	 * @param context
	 * @param imageWidth
	 * @param imageHeight
	 */
	public MketImageResizer(Context context) {
		super(context);
	}

	public static abstract class OnResizeProcess {
		public abstract void run(Bitmap bitmap);
	}
	
	// protected Bitmap processBitmap(String url, int width, int height, int round, boolean resize, boolean crop) {
	@Override
	protected Bitmap processBitmap(String key, int width, int height, int round, boolean resize, boolean crop) {
		return processBitmap(Integer.parseInt(String.valueOf(key)), width, height, round, resize, crop);
	}

	/**
	 * The main processing method. This happens in a background task. In this
	 * case we are just sampling down the bitmap and returning it from a
	 * resource.
	 * 
	 * @param resId
	 * @return
	 */
	protected Bitmap processBitmap(int resId, int width, int height, int round, boolean resize, boolean crop) {
		return decodeSampledBitmapFromResource(_resources, resId, width, height, round, resize, crop);
	}

	/**
	 * Decode and sample down a bitmap from resources to the requested width and
	 * height.
	 * 
	 * @param res
	 *            The resources object containing the image data
	 * @param resId
	 *            The resource id of the image data
	 * @param reqWidth
	 *            The requested width of the resulting bitmap
	 * @param reqHeight
	 *            The requested height of the resulting bitmap
	 * @return A bitmap sampled down from the original with the same aspect
	 *         ratio and dimensions that are equal to or greater than the
	 *         requested width and height
	 */
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight, int round,
			boolean resizeAccurately, boolean cropAccurately) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		Bitmap bitmap = BitmapFactory.decodeResource(res, resId, options);

		if (resizeAccurately && bitmap != null) {
			bitmap = MketImageResizer.resizeAccurately(bitmap, reqWidth, reqHeight, cropAccurately);
		}
		
		// Round the edges of the bitmap
		if(round > 0){
			bitmap = roundBitmapCorner(bitmap, round);
			
		}
		return bitmap;
	}

	/**
	 * Decode and sample down a bitmap from a file to the requested width and
	 * height.
	 * 
	 * @param filename
	 *            The full path of the file to decode
	 * @param reqWidth
	 *            The requested width of the resulting bitmap
	 * @param reqHeight
	 *            The requested height of the resulting bitmap
	 * @return A bitmap sampled down from the original with the same aspect
	 *         ratio and dimensions that are equal to or greater than the
	 *         requested width and height
	 */
	public static Bitmap decodeSampledBitmapFromFile(String filename, int reqWidth, int reqHeight, boolean resizeAccurately,
			boolean cropAccurately) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filename, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		Bitmap bitmap = BitmapFactory.decodeFile(filename, options);

		if (resizeAccurately && bitmap != null) {
			bitmap = MketImageResizer.resizeAccurately(bitmap, reqWidth, reqHeight, cropAccurately);
		}
		return bitmap;
	}

	/**
	 * Decode and sample down a bitmap from a file input stream to the requested
	 * width and height.
	 * 
	 * @param fileDescriptor
	 *            The file descriptor to read from
	 * @param reqWidth
	 *            The requested width of the resulting bitmap
	 * @param reqHeight
	 *            The requested height of the resulting bitmap
	 * @return A bitmap sampled down from the original with the same aspect
	 *         ratio and dimensions that are equal to or greater than the
	 *         requested width and height
	 */
	public static Bitmap decodeSampledBitmapFromDescriptor(FileDescriptor fileDescriptor, int reqWidth, int reqHeight,
			boolean resizeAccurately, boolean cropAccurately) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);

		if (resizeAccurately && bitmap != null) {
			bitmap = MketImageResizer.resizeAccurately(bitmap, reqWidth, reqHeight, cropAccurately);
		}
		return bitmap;
	}

	/**
	 * Calculate an inSampleSize for use in a {@link BitmapFactory.Options}
	 * object when decoding bitmaps using the decode* methods from
	 * {@link BitmapFactory}. This implementation calculates the closest
	 * inSampleSize that will result in the final decoded bitmap having a width
	 * and height equal to or larger than the requested width and height. This
	 * implementation does not ensure a power of 2 is returned for inSampleSize
	 * which can be faster when decoding but results in a larger bitmap which
	 * isn't as useful for caching purposes.
	 * 
	 * @param options
	 *            An options object with out* params already populated (run
	 *            through a decode* method with inJustDecodeBounds==true
	 * @param reqWidth
	 *            The requested width of the resulting bitmap
	 * @param reqHeight
	 *            The requested height of the resulting bitmap
	 * @return The value to be used for inSampleSize
	 */
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int imageHeight = options.outHeight;
		final int imageWidth = options.outWidth;
		int inSampleSize = 1;

		if (reqHeight <= 0) {
			reqHeight = reqWidth * imageHeight / imageWidth;
		}

		if (imageHeight > reqHeight || imageWidth > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) imageHeight / (float) reqHeight);
			final int widthRatio = Math.round((float) imageWidth / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee a final image
			// with both dimensions larger than or equal to the requested height
			// and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

			// This offers some additional logic in case the image has a strange
			// aspect ratio. For example, a panorama may have a much larger
			// width than height. In these cases the total pixels might still
			// end up being too large to fit comfortably in memory, so we should
			// be more aggressive with sample down the image (=larger
			// inSampleSize).

			final float totalPixels = imageWidth * imageHeight;

			// Anything more than 2x the requested pixels we'll sample down
			// further
			final float totalReqPixelsCap = reqWidth * reqHeight * 2;

			while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
				inSampleSize++;
			}
		}
		return inSampleSize;
	}

	public static Bitmap resizeAccurately(Bitmap bitmap, int reqWidth, int reqHeight, boolean centerCrop) {
		if (bitmap == null || reqWidth <= 0) {
			return null;
		}
		int imageWidth = bitmap.getWidth();
		int imageHeight = bitmap.getHeight();

		if (reqWidth > imageWidth) {
			reqWidth = imageWidth;
		}

		if (reqHeight > imageHeight) {
			reqHeight = imageHeight;
		}

		if (reqHeight <= 0) {
			reqHeight = reqWidth * imageHeight / imageWidth;
		}

		int maxSize = getMaximumTextureSize() - 100;
		if (maxSize < reqHeight) {
			reqHeight = maxSize;
			reqWidth = reqHeight * imageWidth / imageHeight;
		}

		// request 길이에 맞춰서 변형시켰을때 실제 길이이다.
		// reqWidth 나 reqHeight 보다 크거나 작을수 있다.
		int outWidth = Math.round(imageWidth * reqHeight / imageHeight);
		int outHegiht = Math.round(imageHeight * reqWidth / imageWidth);


		if (outWidth >= reqWidth) {
			bitmap = Bitmap.createScaledBitmap(bitmap, outWidth, reqHeight, false);
			if (centerCrop) {
				bitmap = Bitmap.createBitmap(bitmap, outWidth / 2 - reqWidth / 2, 0, reqWidth, reqHeight);
			}
		} else {
			bitmap = Bitmap.createScaledBitmap(bitmap, reqWidth, outHegiht, false);
			if (centerCrop) {
				bitmap = Bitmap.createBitmap(bitmap, 0, outHegiht / 2 - reqHeight / 2, reqWidth, reqHeight);
			}
		}

		return bitmap;
	}

	public static HashMap<String, Integer> getDpiSize(Resources resources, int width, int height) {
		return getDpiSize(resources, width, height, DisplayMetrics.DENSITY_XHIGH);
	}

	public static HashMap<String, Integer> getDpiSize(Resources resources, int width, int height, int requestedDensityDpi) {
		HashMap<String, Integer> HashMap = new HashMap<String, Integer>();
		int densityDpi = resources.getDisplayMetrics().densityDpi;

		int returnWidth = Math.round(width * requestedDensityDpi / densityDpi);
		int returnHeight = Math.round(height * requestedDensityDpi / densityDpi);

		HashMap.put("width", returnWidth);
		HashMap.put("height", returnHeight);

		return HashMap;

	}

	private static class _ResizeAsyncTask extends AsyncTask<String, Integer, Bitmap> {
		private WeakReference<Bitmap> _bitmapWeakReference;
		private int _width;
		private int _height;
		private boolean _crop;

		public _ResizeAsyncTask(Bitmap bitmap, int width, int height, boolean crop) {
			_bitmapWeakReference = new WeakReference<Bitmap>(bitmap);
			this._width = width;
			this._height = height;
			this._crop = crop;

		}

		@Override
		protected Bitmap doInBackground(String... params) {
			Bitmap bitmap = _bitmapWeakReference != null ? _bitmapWeakReference.get() : null;

			if (bitmap == null) {
				return null;
			}
			bitmap = MketImageResizer.resizeAccurately(bitmap, _width, _height, _crop);

			return bitmap;
		}

	}

	public static int getMaximumTextureSize() {
		if (MAXIMUM_TEXTURE_SIZE > 0) {
			return MAXIMUM_TEXTURE_SIZE;
		}

		EGL10 egl = (EGL10) EGLContext.getEGL();
		EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);

		// Initialise
		int[] version = new int[2];
		egl.eglInitialize(display, version);

		// Query total number of configurations
		int[] totalConfigurations = new int[1];
		egl.eglGetConfigs(display, null, 0, totalConfigurations);

		// Query actual list configurations
		EGLConfig[] configurationsList = new EGLConfig[totalConfigurations[0]];
		egl.eglGetConfigs(display, configurationsList, totalConfigurations[0], totalConfigurations);

		int[] textureSize = new int[1];
		int maximumTextureSize = 0;

		// Iterate through all the configurations to located the maximum texture
		// size
		for (int i = 0; i < totalConfigurations[0]; i++) {
			// Only need to check for width since opengl textures are always
			// squared
			egl.eglGetConfigAttrib(display, configurationsList[i], EGL10.EGL_MAX_PBUFFER_WIDTH, textureSize);

			// Keep track of the maximum texture size
			if (maximumTextureSize < textureSize[0]) {
				maximumTextureSize = textureSize[0];
			}

			Log.i("GLHelper", Integer.toString(textureSize[0]));
		}

		// Release
		egl.eglTerminate(display);
		Log.i("GLHelper", "Maximum GL texture size: " + Integer.toString(maximumTextureSize));

		MAXIMUM_TEXTURE_SIZE = maximumTextureSize;
		return maximumTextureSize;

	}

	public static Bitmap roundBitmapCorner(Bitmap bitmap, int pixels) {
		if(bitmap == null || pixels <= 0){
			return null;
		}
		
		Log.d("CM", "roundBitmapCorner AHHAHAHAHAHAHAH!!!!!!!!!!!!!!!!!!!11 pixels:"+ pixels);
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = pixels;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}

	

}
