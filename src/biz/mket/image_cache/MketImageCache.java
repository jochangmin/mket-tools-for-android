package biz.mket.image_cache;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.LruCache;
import android.util.Log;
import biz.mket.mkettools.BuildConfig;
import biz.mket.tools.MketTools;


public class MketImageCache {
	// Default memory cache size in kilobytes
	public static final int DEFAULT_MEM_CACHE_SIZE = 1024 * 5; // 5MB

	// Default disk cache size in bytes
	public static final int DEFAULT_DISK_CACHE_SIZE = 1024 * 1024 * 10; // 10MB
	public static final int DISK_CACHE_INDEX = 0;
	public static final int DEFAULT_COMPRESS_QUALITY = 70;
	public static final CompressFormat DEFAULT_COMPRESS_FORMAT = CompressFormat.JPEG;

	public static Object _diskCacheLock = new Object();
	
	// Internal Fields
	public Context _context;
	public DiskLruCache _diskLruCache;
	public LruCache<String, Bitmap> _memLruCache;
	public boolean _diskCacheInProcess = true;
	public String _diskCacheUniqueName;
	public int _diskRequestedCapacity;

	public MketImageCache(Context context) {
		_context = context;
	}

	/**
	 * Find and return an existing ImageCache stored in a {@link RetainFragment}, if not found a new one is created
	 * using the supplied params and saved to a {@link RetainFragment}.
	 */
	public static MketImageCache findOrCreateCache(Context context, FragmentManager fragmentManager) {
		// Search for, or create an instance of the non-UI RetainFragment
		final MketRetainFragment mRetainFragment = MketRetainFragment.findOrCreateRetainFragment(fragmentManager);

		// See if we already have an ImageCache stored in RetainFragment
		MketImageCache mketImageCache = (MketImageCache) mRetainFragment.getObject();

		// No existing ImageCache, create one and store it in RetainFragment
		if (mketImageCache == null) {
			mketImageCache = new MketImageCache(context);
			mRetainFragment.setObject(mketImageCache);
		}
		return mketImageCache;
	}

	public void initMemCache(float maxPercent) {
		int maxSize = getMemCacheSizeByPercent(maxPercent);

		if (BuildConfig.DEBUG) {
			//Log.d("CM", "memCache size : " + String.valueOf(maxSize) + "kb ,max Percent : " + String.valueOf(maxPercent));
		}

		_memLruCache = new LruCache<String, Bitmap>(maxSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				final int bitmapSize = MketTools.getBitmapSize(bitmap) / 1024;
				return bitmapSize == 0 ? 1 : bitmapSize;
			}
		};
	}


	/**
	 * Initialize Disk Cache
	 * 
	 * Initializes the disk cache. Note that this includes disk access so this should not be executed on the main/UI
	 * thread. By default an ImageCache does not initialize the disk cache when it is created, instead you should call
	 * initDiskCache() to initialize it on a background thread.
	 * 
	 * @param uniqueName
	 */
	public void initDiskCache(String uniqueName, int diskMegaSize) {
		_diskCacheUniqueName = uniqueName;
		_diskRequestedCapacity = diskMegaSize;
		long requestedDiskCapacity = Long.valueOf(diskMegaSize*1024*1024);
		
		synchronized (_diskCacheLock) {
			// if diskLruCache has not been initialized yet..
			if (_diskLruCache == null || _diskLruCache.isClosed()) {
				File diskCacheDir = getDiskCacheDir(_context, _diskCacheUniqueName);
				if (diskCacheDir != null) {
					if (BuildConfig.DEBUG) {
						Log.d("CM", "use of DiskCache : " + diskCacheDir.getPath());
					}

					if (!diskCacheDir.exists()) {
						diskCacheDir.mkdirs();
					}
					
					// requested disk cache capacity should leave some free disk for other operations.
					// if it use whole capacity then it is highly likely to break the system down.
					// so I put this code to limit the requted cache capacity to 80% of the whole available disk.
					long usableSpace = MketTools.getUsableSpace(diskCacheDir);
					if(requestedDiskCapacity > usableSpace*0.8){
						requestedDiskCapacity = (long) (usableSpace*0.80);
					}
					
					try {
						_diskLruCache = DiskLruCache.open(diskCacheDir, 1, 1, requestedDiskCapacity);
					} catch (IOException e) {
						e.printStackTrace();
						diskCacheDir = null;
					}
				}
			}

			_diskCacheInProcess = false;
			_diskCacheLock.notifyAll();
		}
	}

	public boolean addBitmapToCache(String key, Bitmap bitmap) {
		if (key == null || bitmap == null) {
			return false;
		}

		// Add to memory cache
		if (_memLruCache != null && _memLruCache.get(key) == null) {
			_memLruCache.put(key, bitmap);
		}

		// check if disk cache has been initialized
		if (_diskLruCache == null) {
			return false;
		}

		// Add to disk cache
		synchronized (_diskCacheLock) {
			final String hashKey = hashKeyForDisk(key);
			OutputStream outputStream = null;

			try {
				DiskLruCache.Snapshot snapshot = _diskLruCache.get(hashKey);
				if (snapshot == null) {
					final DiskLruCache.Editor editor = _diskLruCache.edit(hashKey);
					if (editor != null) {
						outputStream = editor.newOutputStream(DISK_CACHE_INDEX);
						bitmap.compress(DEFAULT_COMPRESS_FORMAT, DEFAULT_COMPRESS_QUALITY, outputStream);
						editor.commit();
						outputStream.close();
					}
				} else {
					snapshot.getInputStream(DISK_CACHE_INDEX).close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (outputStream != null) {
						outputStream.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}

		return true;
	}

	public Bitmap getBitmapFromMemCache(String key) {
		if (_memLruCache != null) {
			final Bitmap memBitmap = _memLruCache.get(key);
			if (memBitmap != null) {
				return memBitmap;
			}
		}
		return null;
	}

	public Bitmap getBitmapFromDiskCache(String key) {
		final String hashKey = hashKeyForDisk(key);
		synchronized (_diskCacheLock) {
			while (_diskCacheInProcess) {
				try {
					_diskCacheLock.wait();
				} catch (InterruptedException e) {
				}
			}

			if (_diskLruCache != null) {
				InputStream inputStream = null;
				try {
					final DiskLruCache.Snapshot snapshot = _diskLruCache.get(hashKey);
					if (snapshot != null) {
						inputStream = snapshot.getInputStream(DISK_CACHE_INDEX);
						if (inputStream != null) {
							final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
							return bitmap;
						}
					}

				} catch (final IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (inputStream != null) {
							inputStream.close();
						}
					} catch (IOException e) {
					}
				}
			}
		}
		return null;
	}

	/**
	 * Sets the memory cache size based on a percentage of the max available VM memory. Eg. setting percent to 0.2 would
	 * set the memory cache to one fifth of the available memory. Throws {@link IllegalArgumentException} if percent is
	 * < 0.05 or > .8. memCacheSize is stored in kilobytes instead of bytes as this will eventually be passed to
	 * construct a LruCache which takes an int in its constructor.
	 * 
	 * This value should be chosen carefully based on a number of factors Refer to the corresponding Android Training
	 * class for more discussion: http://developer.android.com/training/displaying-bitmaps/
	 * 
	 * @param percent Percent of available app memory to use to size memory cache
	 */
	public int getMemCacheSizeByPercent(float percent) {
		if (percent < 0.05f || percent > 0.8f) {
			throw new IllegalArgumentException("setMemCacheSizePercent - percent must be " + "between 0.05 and 0.8 (inclusive)");
		}
		return Math.round(percent * Runtime.getRuntime().maxMemory() / 1024);
	}
	
	public void remove(String key){
		if(_memLruCache != null){
			_memLruCache.remove(key);
		}
		
		synchronized (_diskCacheLock) {
			_diskCacheInProcess = true;
			if(_diskLruCache != null){
				try {
					_diskLruCache.remove(key);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			_diskCacheInProcess = false;
		}
	}

	/**
	 * Clears both the memory and disk cache associated with this ImageCache object. Note that this includes disk access
	 * so this should not be executed on the main/UI thread.
	 */
	public void clearCache() {
		if (_memLruCache != null) {
			_memLruCache.evictAll();
		}

		synchronized (_diskCacheLock) {
			_diskCacheInProcess = true;
			if (_diskLruCache != null && !_diskLruCache.isClosed()) {
				try {
					// Closes the cache and deletes all of its stored values. This will delete all files in the cache
					// directory including files that weren't created by the cache.
					_diskLruCache.delete();
				} catch (IOException e) {
					e.printStackTrace();
				}
				_diskLruCache = null;
				initDiskCache(_diskCacheUniqueName, _diskRequestedCapacity);
			}
		}
	}

	/**
	 * Flushes the disk cache associated with this ImageCache object. Note that this includes disk access so this should
	 * not be executed on the main/UI thread.
	 */
	public void flush() {
		synchronized (_diskCacheLock) {
			if (_diskLruCache != null) {
				try {
					_diskLruCache.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Closes the disk cache associated with this ImageCache object. Note that this includes disk access so this should
	 * not be executed on the main/UI thread.
	 */
	public void close() {
		synchronized (_diskCacheLock) {
			if (_diskLruCache != null) {
				try {
					if (!_diskLruCache.isClosed()) {
						_diskLruCache.close();
						_diskLruCache = null;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Get a usable cache directory (external if available, internal otherwise).
	 * 
	 * @param context The context to use
	 * @param uniqueName A unique directory name to append to the cache dir
	 * @return The cache dir
	 */
	public static File getDiskCacheDir(Context context, String uniqueName) {
		// Check if media is mounted or storage is built-in, if so, try and use external cache dir
		// otherwise use internal cache dir
		boolean isExternalStorageAvailable = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || MketTools.isExternalStorageRemovable();
		String cachePath;
		if (isExternalStorageAvailable) {
			cachePath = MketTools.getExternalCacheDir(context).getPath();
		} else {
			cachePath = context.getCacheDir().getPath();
		}
		return new File(cachePath + File.separator + uniqueName);
	}

	/**
	 * A hashing method that changes a string (like a URL) into a hash suitable for using as a disk filename.
	 */
	public static String hashKeyForDisk(String key) {
		String cacheKey;
		try {
			final MessageDigest mDigest = MessageDigest.getInstance("MD5");
			mDigest.update(key.getBytes());
			cacheKey = bytesToHexString(mDigest.digest());
		} catch (NoSuchAlgorithmException e) {
			cacheKey = String.valueOf(key.hashCode());
		}
		return cacheKey;
	}

	private static String bytesToHexString(byte[] bytes) {
		// http://stackoverflow.com/questions/332079
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(0xFF & bytes[i]);
			if (hex.length() == 1) {
				sb.append('0');
			}
			sb.append(hex);
		}
		return sb.toString();
	}

}
