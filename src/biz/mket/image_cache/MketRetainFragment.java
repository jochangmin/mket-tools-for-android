package biz.mket.image_cache;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

public class MketRetainFragment extends Fragment {
	public final static String _TAG = "MketRetainFragment";

	public Object _object = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	public Object getObject() {
		return _object;
	}

	public void setObject(Object object) {
		_object = object;
	}

	public static MketRetainFragment findOrCreateRetainFragment(FragmentManager fm) {
		MketRetainFragment fragment = (MketRetainFragment) fm.findFragmentByTag(_TAG);
		if (fragment == null) {
			fragment = new MketRetainFragment();
			fm.beginTransaction().add(fragment, _TAG).commitAllowingStateLoss();
		}
		return fragment;

	}

}
