package biz.mket.image_cache;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;

import android.R.integer;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.util.Patterns;
import android.widget.ImageView;

public abstract class MketImageCacheAsync {
	public static final int FADE_IN_TIME = 200;

	protected static final String MESSAGE_CLEAR = "clear";
	protected static final String MESSAGE_INIT_DISK_CACHE = "init_disk_cache";
	protected static final String MESSAGE_FLUSH = "flush";
	protected static final String MESSAGE_CLOSE = "close";

	public Context _context;
	public Resources _resources;
	public MketImageCache _imageCache;

	public Object _pauseWorkerTaskLock = new Object();
	public Bitmap _loadingBitmap = null;
	public boolean _pauseWorkerTask = false;
	public boolean _fadeInBitmap = true;
	public boolean _exitWorkerTaskEarly = false;
	public String _uniqueName;

	public int _diskMegaSize;

	public MketImageCacheAsync(Context context) {
		_context = context;
		_resources = context.getResources();
	}

	public void initImageCache(FragmentManager fm) {
		_imageCache = MketImageCache.findOrCreateCache(_context, fm);
	}

	public void initMemCache(float maxSize) {
		_imageCache.initMemCache(maxSize);
	}

	public void initDiskCache(String uniqueName, int diskMegaSize) {
		_uniqueName = uniqueName;
		_diskMegaSize = diskMegaSize;
		new _CacheAsyncTask().execute(MESSAGE_INIT_DISK_CACHE);
	}

	public String loadImage(String url, ImageView imageView) {
		return loadImage(url, imageView, -1, -1, false, false, null, null);
	}

	public String loadImage(String url, ImageView imageView, int width, int height, boolean resize, boolean crop) {
		return loadImage(url, imageView, width, height, -1, resize, crop, null, null);
	}

	public String loadImage(String url, ImageView imageView, int width, int height, boolean resize, boolean crop,
			HashMap<String, Integer> imageHeights, MketImageCacheCallback callback) {
		return loadImage(url, imageView, width, height, -1, resize, crop, imageHeights, callback);
	}

	public String loadImage(String url, ImageView imageView, int width, int height, int round, boolean resize, boolean crop,
			HashMap<String, Integer> imageHeights, MketImageCacheCallback callback) {
		if (url == null) {
			return null;
		}

		Bitmap bitmap = null;
		String key = _createCacheKey(url, width, height, round, resize, crop);

		if (_imageCache != null && _imageCache._memLruCache != null) {
			// find the bitMap from memCache, when not found, create a
			// BitmapWorkerTask instance,
			// and find the bitmap from DiskCache.
			// when not found in DiskCache, it finds bitMap from subClass using
			// processBitmap()
			bitmap = _imageCache.getBitmapFromMemCache(key);
		}
		if (bitmap != null) {
			// Bitmap found in memory cache
			imageView.setImageBitmap(bitmap);

			// Set the height of the ImageView
			if (imageHeights != null) {
				if (!imageHeights.containsKey(key)) {
					imageHeights.put(key, bitmap.getHeight());
				}
				imageView.getLayoutParams().height = imageHeights.get(key);
			}

			if (callback != null) {
				int recommendedHeight = getRecommendedHeight(bitmap, width);
				callback.callback(bitmap, key, recommendedHeight);
			}

		} else if (cancelPotentialWork(url, imageView)) {

			// BitmapWorkerTask tries to get bitmap from Disk Cache.
			final BitmapWorkerTask task = new BitmapWorkerTask(imageView, width, height, round, resize, crop, imageHeights, callback);
			final AsyncDrawable asyncDrawable = new AsyncDrawable(_resources, _loadingBitmap, task);
			imageView.setImageDrawable(asyncDrawable);

			// NOTE: This uses a custom version of AsyncTask that has been
			// pulled from the
			// framework and slightly modified. Refer to the docs at the top of
			// the class
			// for more info on what was changed.
			task.executeOnExecutor(AsyncTask.DUAL_THREAD_EXECUTOR, url);
		}

		return key;
	}

	/**
	 * memCache나 DiskCache 에서 발견이 안될시.. subclass에서 key를 사용해 bitmap을 찾아낸다.
	 * Subclasses should override this to define any processing or work that
	 * must happen to produce the final bitmap. This will be executed in a
	 * background thread and be long running. For example, you could resize a
	 * large bitmap here, or pull down an image from the network.
	 * 
	 * @param key
	 *            The data to identify which image to process, as provided by
	 * @return The processed bitmap
	 */
	protected abstract Bitmap processBitmap(String key, int width, int height, int round, boolean resize, boolean crop);

	public void setLoadingImage(Bitmap bitmap) {
		_loadingBitmap = bitmap;
	}

	public void setLoadingImage(int resId) {
		_loadingBitmap = BitmapFactory.decodeResource(_resources, resId);
	}

	/**
	 * If set to true, the image will fade-in once it has been loaded by the
	 * background thread.
	 */
	public void setImageFadeIn(boolean fadeIn) {
		_fadeInBitmap = fadeIn;
	}

	public void setExitTasksEarly(boolean exitTasksEarly) {
		_exitWorkerTaskEarly = exitTasksEarly;
	}

	public void clearCacheAsynchronously() {
		new _CacheAsyncTask().execute(MESSAGE_CLEAR);
	}

	public void flushCacheAsynchronously() {
		new _CacheAsyncTask().execute(MESSAGE_FLUSH);
	}

	public void closeCacheAsynchronously() {
		new _CacheAsyncTask().execute(MESSAGE_CLOSE);
	}

	/**
	 * Called when the processing is complete and the final bitmap should be set
	 * on the ImageView.
	 * 
	 * @param imageView
	 * @param bitmap
	 */
	private void _setImageBitmap(ImageView imageView, Bitmap bitmap) {
		if (_fadeInBitmap) {
			// Transition drawable with a transparent drwabale and the final
			// bitmap
			final TransitionDrawable td = new TransitionDrawable(new Drawable[] { new ColorDrawable(android.R.color.transparent),
					new BitmapDrawable(_resources, bitmap) });
			// Set background to loading bitmap
			imageView.setBackgroundDrawable(new BitmapDrawable(_resources, _loadingBitmap));

			imageView.setImageDrawable(td);
			td.startTransition(FADE_IN_TIME);
		} else {
			imageView.setImageBitmap(bitmap);
		}
	}

	public void setPauseWork(boolean pauseWork) {
		synchronized (_pauseWorkerTaskLock) {
			_pauseWorkerTask = pauseWork;
			if (!_pauseWorkerTask) {
				_pauseWorkerTaskLock.notifyAll();
			}
		}
	}

	public void removeSynchronously(String... keys) {
		if (_imageCache == null) {
			return;
		}
		for (String key : keys) {
			_imageCache.remove(key);
		}
	}

	public void remove(String... keys) {
		if (_imageCache != null) {
			BitmapRemoveTask removeTask = new BitmapRemoveTask();
			removeTask.execute(keys);
		}
	}

	public int getRecommendedHeight(Bitmap bitmap, int originalWidth) {
		if (bitmap == null) {
			return -1;
		}
		int recommendedHeight = 0;
		int bitmapWidth = bitmap.getWidth();
		int bitmapHeight = bitmap.getHeight();

		recommendedHeight = originalWidth * bitmapHeight / bitmapWidth;

		int maxSize = MketImageResizer.getMaximumTextureSize();
		if (maxSize < recommendedHeight) {
			recommendedHeight = maxSize;
		}
		return recommendedHeight;
	}

	/**
	 * The actual AsyncTask that will asynchronously process the image.
	 */
	private class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
		private final WeakReference<ImageView> _imageViewReference;
		private MketImageCacheCallback _callback;
		private String _key;
		private int _width;
		private int _height;
		private int _round;
		private boolean _resize;
		private HashMap<String, Integer> _imageHeights;
		private boolean _crop;

		public BitmapWorkerTask(ImageView imageView, int width, int height, int round, boolean resize, boolean crop,
				HashMap<String, Integer> imageHeights, MketImageCacheCallback callback) {
			_imageViewReference = new WeakReference<ImageView>(imageView);
			this._width = width;
			this._height = height;
			this._round = round;
			this._resize = resize;
			this._crop = crop;
			this._imageHeights = imageHeights;
			this._callback = callback;
		}

		/**
		 * Background processing.
		 */
		@Override
		protected Bitmap doInBackground(String... keys) {
			String url = keys[0];
			_key = _createCacheKey(url, _width, _height, _round, _resize, _crop);

			Bitmap bitmap = null;
			// Wait here if work is paused and the task is not cancelled
			synchronized (_pauseWorkerTaskLock) {
				while (_pauseWorkerTask && !isCancelled()) {
					try {
						_pauseWorkerTaskLock.wait();
					} catch (InterruptedException e) {
					}
				}
			}

			// If the image cache is available and this task has not been
			// cancelled by another
			// thread and the ImageView that was originally bound to this task
			// is still bound back
			// to this task and our "exit early" flag is not set then try and
			// fetch the bitmap from
			// the cache
			if (_imageCache != null && _imageCache._diskLruCache != null && !isCancelled() && getAttachedImageView() != null
					&& !_exitWorkerTaskEarly) {
				bitmap = _imageCache.getBitmapFromDiskCache(_key);
			}

			// If the bitmap was not found in the cache and this task has not
			// been cancelled by
			// another thread and the ImageView that was originally bound to
			// this task is still
			// bound back to this task and our "exit early" flag is not set,
			// then call the main
			// process method (as implemented by a subclass)
			// Log.d("CM", "BitmapWorkerTask bitmap :"+ String.valueOf(bitmap));
			// Log.d("CM", "BitmapWorkerTask isCancelled :"+
			// String.valueOf(isCancelled()));
			// Log.d("CM", "BitmapWorkerTask getAttachedImageView :"+
			// String.valueOf(getAttachedImageView()));
			// Log.d("CM", "BitmapWorkerTask _exitWorkerTaskEarly :"+
			// String.valueOf(_exitWorkerTaskEarly));

			if (bitmap == null && !isCancelled() && getAttachedImageView() != null && !_exitWorkerTaskEarly) {
				int ratioWidth = _width;
				int ratioHeight = _height;

				bitmap = processBitmap(url, ratioWidth, ratioHeight, _round, _resize, _crop);

			}

			// If the bitmap was processed and the image cache is available,
			// then add the processed
			// bitmap to the cache for future use. Note we don't check if the
			// task was cancelled
			// here, if it was, and the thread is still running, we may as well
			// add the processed
			// bitmap to our cache as it might be used again in the future
			if (bitmap != null && _imageCache != null) {
				_imageCache.addBitmapToCache(_key, bitmap);
			}

			// Callback
			if (_callback != null && bitmap != null) {
				int recommendHeight = getRecommendedHeight(bitmap, _width);
				_callback.callback(bitmap, _key, recommendHeight);
			}

			_callback = null;
			return bitmap;
		}

		/**
		 * Once the image is processed, associates it to the imageView
		 */
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			// if cancel was called on this task or the "exit early" flag is set
			// then we're done
			if (isCancelled() || _exitWorkerTaskEarly) {
				bitmap = null;
			}

			ImageView imageView = getAttachedImageView();
			if (bitmap != null && imageView != null) {
				_setImageBitmap(imageView, bitmap);
			}

			if (imageView != null && _imageHeights != null && _key != null) {
				// Set the height of the ImageView

				if (!_imageHeights.containsKey(_key)) {
					_imageHeights.put(_key, bitmap.getHeight());
				}
				imageView.getLayoutParams().height = _imageHeights.get(_key);
			}
		}

		@Override
		protected void onCancelled(Bitmap bitmap) {
			super.onCancelled(bitmap);
			synchronized (_pauseWorkerTaskLock) {
				_pauseWorkerTaskLock.notifyAll();
			}
		}

		/**
		 * Returns the ImageView associated with this task as long as the
		 * ImageView's task still points to this task as well. Returns null
		 * otherwise.
		 */
		private ImageView getAttachedImageView() {
			final ImageView imageView = _imageViewReference.get();
			final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

			if (this == bitmapWorkerTask) {
				return imageView;
			}
			return null;
		}
	}

	/**
	 * Just Store Image on Cache That's all
	 */
	private class BitmapRemoveTask extends AsyncTask<String, Integer, String> {
		@Override
		protected String doInBackground(String... keys) {
			removeSynchronously(keys);
			return null;
		}
	}

	protected class _CacheAsyncTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... commands) {
			String command = commands[0];
			if (command.equals(MESSAGE_CLEAR)) {
				_clearCacheInternal();
			} else if (command.equals(MESSAGE_INIT_DISK_CACHE)) {
				_initDiskCacheInternal();
			} else if (command.equals(MESSAGE_FLUSH)) {
				_flushCacheInternal();
			} else if (command.equals(MESSAGE_CLOSE)) {
				_closeCacheInternal();
			}
			return null;
		}
	}

	protected void _initDiskCacheInternal() {
		if (_imageCache != null) {
			_imageCache.initDiskCache(_uniqueName, _diskMegaSize);
		}
	}

	protected void _clearCacheInternal() {
		if (_imageCache != null) {
			_imageCache.clearCache();
		}
	}

	protected void _flushCacheInternal() {
		if (_imageCache != null) {
			_imageCache.flush();
		}
	}

	protected void _closeCacheInternal() {
		if (_imageCache != null) {
			_imageCache.close();
			_imageCache = null;
		}
	}

	protected String _createCacheKey(String url, int width, int height, int round, boolean resize, boolean crop) {
		String key = url + String.valueOf(width + height + round) + String.valueOf(resize) + String.valueOf(crop);
		return String.valueOf(key.hashCode());
	}

	/**
	 * Returns true if the current work has been canceled or if there was no
	 * work in progress on this image view. Returns false if the work in
	 * progress deals with the same data. The work is not stopped in that case.
	 */
	public static boolean cancelPotentialWork(String key, ImageView imageView) {
		final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

		if (bitmapWorkerTask != null) {
			final String workerTaskKey = bitmapWorkerTask._key;
			if (workerTaskKey == null || !workerTaskKey.equals(key)) {
				bitmapWorkerTask.cancel(true);
			} else {
				// The same work is already in progress.
				// Log.d("CM",
				// "cancelPotentialWork - The same work is already in progress");
				return false;
			}
		}
		return true;
	}

	public static boolean isPotentialWork(String key, ImageView imageView) {
		final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

		if (bitmapWorkerTask != null) {
			String workerTaskKey = bitmapWorkerTask._key;
			if (workerTaskKey == null || !workerTaskKey.equals(key)) {
			} else {
				// The same work is already in progress.
				// Log.d("CM",
				// "cancelPotentialWork - The same work is already in progress");
				return true;
			}
		}
		return false;
	}

	/**
	 * Cancels any pending work attached to the provided ImageView.
	 * 
	 * @param imageView
	 */
	public static void cancelWorkerTask(ImageView imageView) {
		final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
		if (bitmapWorkerTask != null) {
			bitmapWorkerTask.cancel(true);
		}
	}

	/**
	 * @param imageView
	 *            Any imageView
	 * @return Retrieve the currently active work task (if any) associated with
	 *         this imageView. null if there is no such task.
	 */
	private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
		if (imageView != null) {
			final Drawable drawable = imageView.getDrawable();
			if (drawable instanceof AsyncDrawable) {
				final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
				return asyncDrawable.getBitmapWorkerTask();
			}
		}
		return null;
	}

	/**
	 * A custom Drawable that will be attached to the imageView while the work
	 * is in progress. Contains a reference to the actual worker task, so that
	 * it can be stopped if a new binding is required, and makes sure that only
	 * the last started worker process can bind its result, independently of the
	 * finish order.
	 */
	private static class AsyncDrawable extends BitmapDrawable {
		private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

		public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
			super(res, bitmap);
			bitmapWorkerTaskReference = new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
		}

		public BitmapWorkerTask getBitmapWorkerTask() {
			return bitmapWorkerTaskReference.get();
		}
	}

}
