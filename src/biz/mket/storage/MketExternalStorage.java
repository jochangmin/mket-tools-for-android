package biz.mket.storage;

import java.io.File;
import java.lang.ref.WeakReference;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;
import biz.mket.image_cache.AsyncTask;
import biz.mket.image_cache.MketImageResizer;

public class MketExternalStorage {
	public final static int MODE_EXTERNAL_STORAGE = MketExternalFile.MODE_EXTERNAL_FILE;
	public final static int MODE_EXTERNAL_CACHE = MketExternalFile.MODE_EXTERNAL_CACHE_FILE;
	public final static int MODE_EXTERNAL_PUBLIC = MketExternalFile.MODE_EXTERNAL_PUBLIC;

	private static String _MODE_STORE_BITMAP = "store_bitmap";
	private static String _MODE_DELETE = "delete";
	private static String _MODE_LOAD_IMAGE = "load_image";

	public boolean external_storage_available = false;
	public boolean external_storage_writable = false;

	private Context _context;
	private boolean _resize = false;
	private boolean _cropAccurately = false;
	private int _mode;
	private int _requestedWidth;
	private int _requestedHeight;
	

	public MketExternalStorage(Context context, int mode) {
		_context = context;
		_mode = mode;
		checkExternalStorage();
	}

	public void checkExternalStorage() {
		String exteralStorageState = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(exteralStorageState)) {
			// We can read and write the media
			external_storage_available = true;
			external_storage_writable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(exteralStorageState)) {
			// We can only read the media
			external_storage_available = true;
			external_storage_writable = false;
		} else {
			// Something is wrong. It may be one of many other states
			// ,but all we need to know is we can neither read nor write
			external_storage_available = false;
			external_storage_writable = false;
		}
	}
	
	

	public void deleteFileAsynchronously(String fileName) {
		if (external_storage_available) {
			MketExternalFile exFile = new MketExternalFile(_context, fileName, _mode);
			ExternalStorageTask task = new ExternalStorageTask(_MODE_DELETE, exFile);
			task.execute();
		}
	}

	public void loadImage(ImageView imageView, String fileName) {
		MketExternalFile exFile = new MketExternalFile(_context, fileName, _mode);
		loadImage(imageView, exFile);
	}
	
	public void loadImage(ImageView imageView, MketExternalFile exFile){
		if (external_storage_available) {
			ExternalStorageTask task = new ExternalStorageTask(_MODE_LOAD_IMAGE, exFile, imageView);
			task.execute();
		}
	}
	

	public void storeAsynchronously(String fileName, Bitmap bitmap) {
		MketExternalFile exFile = new MketExternalFile(_context, fileName, _mode);
		storeAsynchronously(exFile, bitmap);
	}
	
	public void storeAsynchronously(MketExternalFile exFile, Bitmap bitmap) {
		if (external_storage_available && external_storage_writable) {
			ExternalStorageTask task = new ExternalStorageTask(_MODE_STORE_BITMAP, exFile, bitmap);
			task.execute();
		}
	}

	public MketExternalFile storeSynchronously(String fileName, Bitmap bitmap) {
		if (external_storage_available && external_storage_writable) {
			MketExternalFile exFile = new MketExternalFile(_context, fileName, _mode);
			
			Log.d("CM", "하하하하 :"+ exFile.getFile().getAbsolutePath());
			
			if (_resize) {
				bitmap = MketImageResizer.resizeAccurately(bitmap, _requestedWidth, _requestedHeight, _cropAccurately);
			}
			exFile.write(bitmap);
			return exFile;
		}
		return null;
	}

	public void setResizeSize(int width, int height, boolean cropAccurately) {
		_resize = true;
		_cropAccurately = cropAccurately;
		_requestedWidth = width;
		_requestedHeight = height;
	}

	protected class ExternalStorageTask extends AsyncTask<String, Integer, MketExternalFile> {
		private MketExternalFile _file;
		private Bitmap _bitmap;
		private String _mode;
		private WeakReference<ImageView> _imageViewWeakRef;

		public ExternalStorageTask(String mode, MketExternalFile file) {
			_mode = mode;
			_file = file;
		}

		public ExternalStorageTask(String mode, MketExternalFile file, Bitmap bitmap) {
			_mode = mode;
			_file = file;
			_bitmap = bitmap;
		}

		public ExternalStorageTask(String mode, MketExternalFile file, ImageView imageView) {
			_mode = mode;
			_file = file;
			_imageViewWeakRef = new WeakReference<ImageView>(imageView);
		}

		@Override
		protected MketExternalFile doInBackground(String... mode) {
			if (_mode == null || _file == null) {
				return null;
			}
			if (_mode.equals(_MODE_STORE_BITMAP) && _bitmap != null) {
				process_store_bitmap(_file, _bitmap);
			} else if (_mode.equals(_MODE_DELETE)) {
				process_delete(_file);
			} else if (_mode.equals(_MODE_LOAD_IMAGE)) {
				_bitmap = process_load_image(_file);
			}

			return _file;
		}

		@Override
		protected void onPostExecute(MketExternalFile file) {
			super.onPostExecute(file);
			
			Log.d("CM", "onPostExecute");
			if (_mode.equals(_MODE_LOAD_IMAGE)) {
				Log.d("CM", "onPostExecute _MODE_LOAD_IMAGE");
				ImageView imageView = _imageViewWeakRef.get();
				Log.d("CM", "onPostExecute imageView "+ String.valueOf(imageView));
				Log.d("CM", "onPostExecute _bitmap "+ String.valueOf(_bitmap));
				if (imageView != null && _bitmap != null) {
					imageView.setBackgroundDrawable(new BitmapDrawable(_bitmap));
				}
			}

			if (file != null) {
				file.close();
			}
			if (_file != null) {
				// _fileWeakRef.clear();
				_file.close();
				_file = null;
			}
			if (_file != null) {
				// _bitmapWeakRef.clear();
				_bitmap = null;
			}
			if (_imageViewWeakRef != null) {
				_imageViewWeakRef.clear();
			}
		}
	}

	protected Bitmap process_store_bitmap(MketExternalFile exFile, Bitmap bitmap) {
		if (_resize) {
			bitmap = MketImageResizer.resizeAccurately(bitmap, _requestedWidth, _requestedHeight, _cropAccurately);
		}
		
		exFile.write(bitmap);
		return bitmap;
	}

	protected Bitmap process_load_image(MketExternalFile exFile) {
		File file = exFile.getFile();
		Log.d("CM", "process_load_image file"+ String.valueOf(file));
		Log.d("CM", "process_load_image file.exists"+ String.valueOf(file.exists()));
		if (file == null || !file.exists()) {
			return null;
		}
		Log.d("CM", "process_load_image file.getPath()"+ String.valueOf(file.getPath()));
		Bitmap bitmap = MketImageResizer.decodeSampledBitmapFromFile(file.getPath(), _requestedWidth, _requestedHeight, _resize, _cropAccurately);
		return bitmap;
	}

	protected void process_delete(MketExternalFile exFile) {
		exFile.delete();
	}
	
	
	
	/**
	 * A hashing method that changes a string (like a URL) into a hash suitable for using as a disk filename.
	 */
	public static String createHashKeyForDisk(String key) {
		String cacheKey;
		try {
			final MessageDigest mDigest = MessageDigest.getInstance("MD5");
			mDigest.update(key.getBytes());
			cacheKey = bytesToHexString(mDigest.digest());
		} catch (NoSuchAlgorithmException e) {
			cacheKey = String.valueOf(key.hashCode());
		}
		return cacheKey;
	}

	private static String bytesToHexString(byte[] bytes) {
		// http://stackoverflow.com/questions/332079
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(0xFF & bytes[i]);
			if (hex.length() == 1) {
				sb.append('0');
			}
			sb.append(hex);
		}
		return sb.toString();
	}
	
	public static void refreshGalleryApp(Context context){
		context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://"+ Environment.getExternalStorageDirectory())));
	}
	

	public static void __debug_deleteAppFiles(Context context) {
		// delete all files on /storage/sdcard0/Android/data/tv.mket.style/files
		MketExternalFile.deleteRecursive(context.getExternalFilesDir(null));
		MketExternalFile.deleteRecursive(context.getExternalCacheDir());
	}

	public static void deleteCacheFile(Context context, String fileName) {
		// delete a file on /storage/sdcard0/Android/data/tv.mket.style/files
		// if you want to delete all files call it like "deleteCacheFile(this, "")"
		if(fileName != null && fileName.length() >= 1){
			MketExternalFile.deleteRecursive(new File(context.getExternalCacheDir(), fileName));			
		}
	}
	

}
