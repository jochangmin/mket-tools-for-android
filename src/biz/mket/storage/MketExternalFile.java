package biz.mket.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

public class MketExternalFile {
	public final static int MODE_EXTERNAL_FILE = 0;
	public final static int MODE_EXTERNAL_CACHE_FILE = 1;
	public final static int MODE_EXTERNAL_PUBLIC = 2;

	private static Object _storageLock = new Object();

	private Context _context;
	private String _fileName;
	private File _file;

	public MketExternalFile(Context context, String fileName, int mode) {
		_context = context;
		_fileName = fileName;
		if (mode == MODE_EXTERNAL_FILE) {
			_file = new File(_context.getExternalFilesDir(null), fileName);
		} else if (mode == MODE_EXTERNAL_CACHE_FILE) {
			_file = new File(_context.getExternalCacheDir(), fileName);
		} else if (mode == MODE_EXTERNAL_PUBLIC) {
			_file = new File(Environment.getExternalStorageDirectory(), fileName);
		}
		
	}

	public boolean exists() {
		return _file.exists();
	}

	public String getAbsolutePath() {
		if (_file != null) {
			return _file.getAbsolutePath();
		}
		return null;
	}

	public File getFile() {
		return _file;
	}
	
	public FileOutputStream getFileOutputStream(){
		try {
			return new FileOutputStream(_file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public FileInputStream getFileInputStream(){
		
		try {
			return new FileInputStream(_file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void write(Bitmap bitmap) {
		synchronized (_storageLock) {
			String hashKey = hashKeyForDisk(bitmap.toString());

			// make directory
			/**
			 * File directory = openFile(location); if(!directory.exists()){ directory.mkdirs(); }
			 */

			// create directories
			if (!_file.getParentFile().exists()) {
				_file.getParentFile().mkdirs();
			}

			// create a new file if not found
			if (!_file.exists()) {
				try {
					_file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			// write data
			FileOutputStream out;
			try {
				out = new FileOutputStream(_file);
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
				out.flush();
				out.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			_storageLock.notifyAll();
		}

	}

	public void delete() {
		synchronized (_storageLock) {
			if (_file != null) {
				deleteRecursive(_file);
			}
			_storageLock.notifyAll();
		}
	}

	public void close() {
		_context = null;
		_fileName = null;
		_file = null;
	}

	public static void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory == null) {
			return;
		}
		synchronized (_storageLock) {
			if (fileOrDirectory.isDirectory())
				if (fileOrDirectory != null && fileOrDirectory.listFiles() != null)
					for (File child : fileOrDirectory.listFiles()) {
						deleteRecursive(child);
					}
			fileOrDirectory.delete();
			_storageLock.notifyAll();
		}
	}

	public static String join(String path1, String path2) {
		File file1 = new File(path1);
		File file2 = new File(file1, path2);
		return file2.getPath();
	}

	/**
	 * A hashing method that changes a string (like a URL) into a hash suitable for using as a disk filename.
	 */
	public static String hashKeyForDisk(String key) {
		String cacheKey;
		try {
			final MessageDigest mDigest = MessageDigest.getInstance("MD5");
			mDigest.update(key.getBytes());
			cacheKey = bytesToHexString(mDigest.digest());
		} catch (NoSuchAlgorithmException e) {
			cacheKey = String.valueOf(key.hashCode());
		}
		return cacheKey;
	}

	private static String bytesToHexString(byte[] bytes) {
		// http://stackoverflow.com/questions/332079
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(0xFF & bytes[i]);
			if (hex.length() == 1) {
				sb.append('0');
			}
			sb.append(hex);
		}
		return sb.toString();
	}

	public static void createParentDirectory(File file) {
		File parentFile = file.getParentFile();
		if (!parentFile.exists()) {
			parentFile.mkdirs();
		}
	}

	public static File createCachedFile(Context context, String prefixDirectory, String format) {
		String timestamp = new SimpleDateFormat("yyyMMdd_HHmmss_SSS").format(new Date());
		String filePath = new File(prefixDirectory, timestamp + "." + format).getPath();
		return new File(context.getExternalCacheDir(), filePath);
	}

	/**
	 * boolean hasExternalStoragePrivateFile() { // Get path for the file on external storage. If external // storage is
	 * not currently mounted this will fail. File file = new File(getExternalFilesDir(null), "DemoFile.jpg"); if (file
	 * != null) { return file.exists(); } return false; }
	 */

}
