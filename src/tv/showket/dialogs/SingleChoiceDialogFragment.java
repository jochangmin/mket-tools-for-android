package tv.showket.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;


public class SingleChoiceDialogFragment extends DialogFragment{
	private String _title;
	private String[] _items;
	private OnClickListener _clickListener;
	
	public static SingleChoiceDialogFragment newInstance(String title, String[] items, OnClickListener clickListener){
		SingleChoiceDialogFragment instance = new SingleChoiceDialogFragment();
		instance.setTitle(title);
		instance.setItems(items);
		instance.setOnClickListener(clickListener);
		return instance;
	}
	
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
		b.setTitle(_title);
		b.setSingleChoiceItems(_items, 0, _clickListener);
		return b.create();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		_title = null;
		_items = null;
		_clickListener = null;
	}
	

	public void setTitle(String title){
		_title = title;
	}
	
	public void setItems(String[] items){
		_items = items;
	}
	
	public void setOnClickListener(OnClickListener clickListener){
		_clickListener = clickListener;
	}

}
