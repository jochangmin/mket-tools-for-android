package tv.showket.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;

public class YesOrNoDialogFragment extends DialogFragment{
	public String message; 
	public String positiveMessage;
	public String negativeMessage;
	public DialogInterface.OnClickListener positiveListener;
	public DialogInterface.OnClickListener negativeListener;
	
	public Dialog onCreateDialog(android.os.Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(message);
		builder.setPositiveButton(positiveMessage, positiveListener);
		builder.setNegativeButton(negativeMessage, negativeListener);
		return builder.create();
	}


}
