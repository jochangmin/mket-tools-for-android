package tv.showket.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.InputFilter;
import android.widget.EditText;
import android.widget.LinearLayout;
import biz.mket.mkettools.R;

public class MketPromptDialogFragment extends DialogFragment {
	public static interface PromptDoneListener {
		public void onPromptDone(String promptValue);
	}

	public String title;
	public Integer inputType;
	public String defaultValue;
	public Integer maxLength;

	protected DialogInterface.OnClickListener _positiveListener;
	protected DialogInterface.OnClickListener _negativeListener;
	protected OnCancelListener _cancelListener;
	protected PromptDoneListener _promptDoneListener;

	protected EditText promptView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);

		LinearLayout rootView = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.mket_prompt_dialog_fragment_layout, null);
		promptView = (EditText) rootView.findViewById(R.id.mket_prompt_dialog_edittext);

		if (inputType != null) {
			promptView.setInputType(inputType);
		}
		if (defaultValue != null) {
			promptView.setText(defaultValue);
		}
		if (maxLength != null) {
			InputFilter[] fArray = new InputFilter[1];
			fArray[0] = new InputFilter.LengthFilter(maxLength);
			promptView.setFilters(fArray);
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		if(title != null){
			builder.setTitle(title);			
		}
		builder.setView(rootView);
		builder.setPositiveButton("네", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (_positiveListener != null) {
					_positiveListener.onClick(dialog, which);
				}

				if (_promptDoneListener != null) {
					_promptDoneListener.onPromptDone(promptView.getText().toString());
				}
			}
		});
		builder.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (_negativeListener != null) {
					_negativeListener.onClick(dialog, which);
				}
			}
		});
		builder.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				if (_cancelListener != null) {
					_cancelListener.onCancel(dialog);
				}
			}
		});
		return builder.create();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		title = null;
		inputType = null;

		promptView = null;

		_positiveListener = null;
		_negativeListener = null;
		_cancelListener = null;

	}

	public void listenPositiveButton(DialogInterface.OnClickListener listener) {
		this._positiveListener = listener;
	}

	public void listenNegativeButton(DialogInterface.OnClickListener listener) {
		this._negativeListener = listener;
	}

	public void listenCancelButton(OnCancelListener listener) {
		this._cancelListener = listener;
	}

	public void listenPromptDone(PromptDoneListener listener) {
		this._promptDoneListener = listener;
	}

}
